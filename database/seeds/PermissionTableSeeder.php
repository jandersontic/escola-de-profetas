<?php

use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->truncate();
        
        $transactions = DB::table('transactions')->where('module', 'Controle')->where('private', 1)->get();
        
        if (count($transactions)) {
            foreach ($transactions as $transaction) {
                DB::table('permissions')->insert(['group_id' => 1, 'transaction_id' => $transaction->id]);
            }
        }
    }
}
