<?php

use Illuminate\Database\Seeder;

class GroupTransactionTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $groupTransactions = [
            ['name' => 'Site', 'icon' => 'fa fa-home fa-fw', 'order' => 1],
            ['name' => 'Controle', 'icon' => 'fa fa-wrench fa-fw', 'order' => 2],
            ['parent_id' => 1, 'name' => 'Banners', 'order' => 1],
            ['parent_id' => 1, 'name' => 'Mensagens', 'order' => 2],
            ['parent_id' => 1, 'name' => 'Eventos', 'order' => 3],
            ['parent_id' => 1, 'name' => 'Preletores', 'order' => 4],
            ['parent_id' => 1, 'name' => 'Inscrições', 'order' => 5],
            ['parent_id' => 1, 'name' => 'Galeria', 'order' => 6],
            ['parent_id' => 1, 'name' => 'Programação', 'order' => 7],
            ['parent_id' => 1, 'name' => 'Contato', 'order' => 8],//10
            ['parent_id' => 1, 'name' => 'Como Chegar', 'order' => 9],
            ['parent_id' => 2, 'name' => 'Grupos', 'order' => 1],//12
            ['parent_id' => 2, 'name' => 'Usuários', 'order' => 2],
            ['parent_id' => 2, 'name' => 'Permissões', 'order' => 3],//14
            ['parent_id' => 2, 'name' => 'Históricos', 'order' => 4],
        ];

        DB::table('group_transaction')->truncate();

        foreach ($groupTransactions as $groupTransaction) {
            DB::table('group_transaction')->insert($groupTransaction);
        }
    }

}
