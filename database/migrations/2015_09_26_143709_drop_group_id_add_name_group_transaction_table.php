<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropGroupIdAddNameGroupTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('group_transaction', function (Blueprint $table) {
            $table->dropColumn(['group_id', 'transaction_id']);
            $table->string('name', 150)->after('id');
            $table->string('icon', 150)->nullable()->after('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('group_transaction', function (Blueprint $table) {
            //
        });
    }
}
