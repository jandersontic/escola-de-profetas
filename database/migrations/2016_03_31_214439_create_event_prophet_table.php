<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventProphetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_prophet', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('event_id')->unsigned();
            $table->foreign('event_id')->references('id')->on('events');
            $table->integer('prophet_id')->unsigned();
            $table->foreign('prophet_id')->references('id')->on('prophets');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('event_prophet', function (Blueprint $table) {
            $table->dropForeign('event_prophet_event_id_foreign');
            $table->dropForeign('event_prophet_prophet_id_foreign');
        });
    }
}
