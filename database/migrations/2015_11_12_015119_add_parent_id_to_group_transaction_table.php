<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddParentIdToGroupTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('group_transaction', function (Blueprint $table) {
            $table->integer('parent_id')->nullable()->unsigned()->after('id');
            $table->integer('order')->nullable()->after('icon');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('group_transaction', function (Blueprint $table) {
            //
        });
    }
}
