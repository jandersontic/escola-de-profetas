<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAddressToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('graduation')->after('email')->nullable();
            $table->date('birthdate')->after('graduation')->nullable();
            $table->text('address')->after('birthdate')->nullable();
            $table->string('number', 15)->after('address')->nullable();
            $table->string('district', 200)->after('number')->nullable();
            $table->string('postal_code', 8)->after('district')->nullable();
            $table->string('city', 300)->after('postal_code')->nullable();
            $table->string('state', 150)->after('city')->nullable();
            $table->string('phone', 15)->after('state')->nullable();
            $table->string('celphone', 15)->after('phone')->nullable();
            $table->tinyInteger('evangelic')->after('celphone')->nullable()->default(0);
            $table->tinyInteger('conversion')->after('evangelic')->nullable();
            $table->tinyInteger('water_batism')->after('conversion')->nullable()->default(0);
            $table->tinyInteger('spirit_batism')->after('water_batism')->nullable()->default(0);
            $table->string('church', 255)->after('spirit_batism')->nullable();
            $table->string('bishop', 255)->after('church')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
