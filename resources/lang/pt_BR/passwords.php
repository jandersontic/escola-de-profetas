<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'As senhas devem ter pelo menos seis caracteres e combinar com a confirmação.',
    'reset' => 'Sua senha foi redefinida!',
    'sent' => 'Enviamos por e-mail as instruções de recuperação de senha!',
    'token' => 'Este token de alteração de senha está inválido.',
    'user' => "Não encontramos um usuário com esse endereço de e-mail.",

];
