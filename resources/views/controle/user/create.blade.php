@extends('layouts.controle')

@section('title', 'Criar um novo usuário')

@section('sidebar')
    @parent

@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Criar um novo usuário</h1>
            </div>
            <!-- /.col-lg-12 -->
            <div class="col-lg-12">
                @include('includes.mensagem')
                <form action="{{ route('controle.user.store') }}" class="form-horizontal" method="post">
                    {!! csrf_field() !!}

                    <div class="form-group">
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <label class="control-label" for="name">Nome*</label>
                            <input value="{{ old('name') or '' }}" id="name" name="name" autofocus placeholder="Nome do Usuário" class="form-control input-lg" required="" type="text">
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <label class="control-label" for="email">E-mail*</label>
                            <input value="{{ old('email') or '' }}" id="email" name="email" placeholder="E-mail do Usuário" class="form-control input-lg" required="" type="text">
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <label class="control-label" for="password">Senha*</label>
                            <input id="password" name="password" required class="form-control input-lg" type="password">
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <label class="control-label" for="password">Confirmar Senha*</label>
                            <input id="password_confirmation" name="password_confirmation" required class="form-control input-lg" type="password">
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                            <label class="control-label" for="group_id">Grupo*</label>
                            <select name="group_id" id="group_id" class="form-control input-lg" required>
                                <option value="0">Selecione</option>
                                @forelse($groups as $group)
                                    <option {{ !empty(old('group_id')) && old('group_id') == $group->id ? 'selected' : '' }} value="{{ $group->id }}">{{ $group->name }}</option>
                                @empty
                                    <optgroup label="Nenhum grupo cadastrado"></optgroup>
                                @endforelse
                            </select>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <label class="control-label" for="birthdate">Data de nascimento</label>
                            <input id="birthdate" name="birthdate" value="{{ old('birthdate') or '' }}" class="form-control input-lg datepicker" type="text">
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <label class="control-label" for="postal_code">Código postal</label>
                            <input id="postal_code" name="postal_code" value="{{ old('postal_code') or '' }}" class="form-control input-lg cep" type="text">
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <label class="control-label" for="number">Número</label>
                            <input id="number" name="number" value="{{ old('number') or '' }}" class="form-control input-lg" type="text">
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <label class="control-label" for="address">Endereço</label>
                            <input id="address" name="address" value="{{ old('address') or '' }}" class="form-control input-lg" type="text">
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <label class="control-label" for="district">Bairro</label>
                            <input id="district" name="district" value="{{ old('district') or '' }}" class="form-control input-lg" type="text">
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <label class="control-label" for="city">Cidade</label>
                            <input id="city" name="city" value="{{ old('city') or '' }}" class="form-control input-lg" type="text">
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                            <label class="control-label" for="state">Estado</label>
                            <input id="state" name="state" value="{{ old('state') or '' }}" class="form-control input-lg" type="text">
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                            <label class="control-label" for="graduation">Escolaridade</label>
                            <input id="graduation" name="graduation" value="{{ old('graduation') or '' }}" class="form-control input-lg" type="text">
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                            <label class="control-label" for="phone">Telefone</label>
                            <input id="phone" name="phone" value="{{ old('phone') or '' }}" class="form-control input-lg celphone" type="text">
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                            <label class="control-label" for="celphone">Celular</label>
                            <input id="celphone" name="celphone" value="{{ old('celphone') or '' }}" class="form-control input-lg celphone" type="text">
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                            <label class="control-label" for="evangelic">Confessou Jesus?*</label>
                            <select name="evangelic" id="evangelic" class="form-control input-lg" required>
                                <option value="">Selecione</option>
                                <option {{ !empty(old('evangelic')) && old('evangelic') == 1 ? 'selected' : '' }} value="1">Sim</option>
                                <option {{ !empty(old('evangelic')) && old('evangelic') == 0 ? 'selected' : '' }} value="0">Não</option>
                            </select>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                            <label class="control-label" for="conversion">Tempo de conversão</label>
                            <input id="conversion" name="conversion" value="{{ old('conversion') or '' }}" class="form-control input-lg" type="text">
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                            <label class="control-label" for="water_batism">Batizado nas águas?*</label>
                            <select name="water_batism" id="water_batism" class="form-control input-lg" required>
                                <option value="">Selecione</option>
                                <option {{ !empty(old('water_batism')) && old('water_batism') == 1 ? 'selected' : '' }} value="1">Sim</option>
                                <option {{ !empty(old('water_batism')) && old('water_batism') == 0 ? 'selected' : '' }} value="0">Não</option>
                            </select>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                            <label class="control-label" for="spirit_batism">Batizado no espírito?*</label>
                            <select name="spirit_batism" id="spirit_batism" class="form-control input-lg" required>
                                <option value="">Selecione</option>
                                <option {{ !empty(old('spirit_batism')) && old('spirit_batism') == 1 ? 'selected' : '' }} value="1">Sim</option>
                                <option {{ !empty(old('spirit_batism')) && old('spirit_batism') == 0 ? 'selected' : '' }} value="0">Não</option>
                            </select>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-6">
                            <label class="control-label" for="church">Igreja</label>
                            <input id="church" name="church" value="{{ old('church') or '' }}" class="form-control input-lg" type="text">
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-6">
                            <label class="control-label" for="bishop">Nome do seu pastor</label>
                            <input id="bishop" name="bishop" value="{{ old('bishop') or '' }}" class="form-control input-lg" type="text">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label class="control-label" for="save"></label>
                            <a href="{{ route('controle.user.index') }}" class="btn btn-default btn-lg">
                                <i class="fa fa-arrow-left fa-fw"></i> Voltar
                            </a>
                            @if (\App\Permission::verificaPermissao('controle.user.store', \Auth::user()->group->id))
                                <button type="submit" id="save" name="save" class="btn btn-primary btn-lg">
                                    <i class="fa fa-save fa-fw"></i> Salvar
                                </button>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
@endsection