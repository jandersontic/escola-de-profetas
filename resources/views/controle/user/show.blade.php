@extends('layouts.controle')

@section('title', 'Ver detalhe do usuário')

@section('sidebar')
    @parent

@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Ver detalhe do usuário</h1>
            </div>
            <!-- /.col-lg-12 -->
            <div class="col-lg-12">
                @include('includes.mensagem')

                    <div class="form-group">
                        <label class="col-md-1 control-label" for="name">Nome</label>
                        <div class="col-md-4">
                            <div class="form-control input-md">{{ $user->name or '' }}</div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label" for="email">E-mail</label>
                        <div class="col-md-4">
                            <div class="form-control input-md">{{ $user->email or '' }}</div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label" for="group_id">Grupo</label>
                        <div class="col-md-4">
                            <div class="form-control input-md">{{ $user->group->name or '' }}</div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label"></label>
                        <div class="col-md-8">
                            <a href="{{ route('controle.user.index') }}" class="btn btn-danger">Voltar</a>
                        </div>
                    </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
@endsection