@extends('layouts.controle')

@section('title', 'Listar Usuários')

@section('sidebar')
@parent

@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Usuários</h1>
        </div>
    </div>  
    <div class="row margin-bottom-15">
        <div class="col-lg-12">
            @include('includes.mensagem')
            @if (\App\Permission::verificaPermissao('controle.user.create', \Auth::user()->group->id))
            <a href="{{ route('controle.user.create') }}" class="btn btn-primary btn-lg right">
                <i class="fa fa-plus fa-fw"></i>Adicionar
            </a>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive">
                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Grupo</th>
                            <th>Opções</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($users as $user)
                        <tr>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->group->name }}</td>
                            <td>
                                @if (\App\Permission::verificaPermissao('controle.user.edit', \Auth::user()->group->id))
                                <a href="{{ route('controle.user.edit', $user->id) }}" class="btn btn-primary btn-lg"><i class="fa fa-pencil fa-fw"></i>Editar</a>
                                @endif
                                @if (\App\Permission::verificaPermissao('controle.user.destroy', \Auth::user()->group->id))
                                <a href="{{ route('controle.user.destroy', $user->id) }}" class="btn btn-danger btn-lg delete-item" data-toggle="modal" data-target="#deleteItemModal"><i class="fa fa-trash fa-fw"></i>Excluir</a>
                                @endif
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="2">
                                Nenhum registro encontrado!
                            </td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.col-lg-12 -->
        <div class="col-lg-12">
            {!! $users->render() !!}
        </div>
    </div>
    <!-- /.row -->
</div>
@include('includes.modal')
<!-- /.container-fluid -->
@endsection