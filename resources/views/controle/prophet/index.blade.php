@extends('layouts.controle')

@section('title', 'Listar preletores')
@section('styles')
@endsection
@section('sidebar')
@parent

@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Preletores</h1>
        </div>
    </div>  
    <div class="row">
        <div class="col-lg-12">
            @include('includes.mensagem')
            @if (\App\Permission::verificaPermissao('controle.prophet.create', \Auth::user()->group->id))
            <a href="{{ route('controle.prophet.create') }}" class="btn btn-primary btn-lg right">
                <i class="fa fa-plus fa-fw"></i>Adicionar
            </a>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive">
                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th>Imagem</th>
                            <th>Nome</th>
                            <th>Opções</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($prophets as $prophet)
                        <tr>
                            <td>
                                @if(is_file(public_path() . '/data/preletor/cut-' . $prophet->image))
                                <img src="/data/preletor/cut-{{ $prophet->image }}" class="img-responsive list-img-small" alt="{{ $prophet->name }}" />
                                @elseif(is_file(public_path() . '/data/preletor/' . $prophet->image))
                                <img src="/data/preletor/{{ $prophet->image }}" class="img-responsive list-img-small" alt="{{ $prophet->name }}" />
                                @endif
                            </td>
                            <td>{{ $prophet->name }}</td>
                            <td>
                                @if (\App\Permission::verificaPermissao('controle.prophet.edit', \Auth::user()->group->id))
                                <a href="{{ route('controle.prophet.edit', $prophet->id) }}" class="btn btn-primary btn-lg">
                                    <i class="fa fa-pencil fa-fw"></i>Editar
                                </a>
                                @endif
                                @if (\App\Permission::verificaPermissao('controle.prophet.destroy', \Auth::user()->group->id))
                                <a href="{{ route('controle.prophet.destroy', $prophet->id) }}" class="btn btn-danger btn-lg delete-item" data-toggle="modal" data-target="#deleteItemModal">
                                    <i class="fa fa-trash fa-fw"></i>Excluir
                                </a>
                                @endif
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="2">
                                Nenhum registro encontrado!
                            </td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@include('includes.modal')
@endsection
@section('scripts')
@endsection