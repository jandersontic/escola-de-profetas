@extends('layouts.controle')

@section('title', 'Editar preletor')

@section('sidebar')
@parent

@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Editar preletor</h1>
        </div>
        <!-- /.col-lg-12 -->
        <div class="col-lg-12">
            @include('includes.mensagem')
            <form action="{{ route('controle.prophet.update', $id) }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <div class="form-group">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        @if(is_file(public_path() . '/data/preletor/cut-' . $prophet->image))
                        <label class="control-label" for="name">Imagem cortada</label>
                        <img src="/data/preletor/cut-{{ $prophet->image }}" class="img-responsive detail-img-small" alt="{{ $prophet->name or '' }}" title="{{ $prophet->name or '' }}" />
                        @elseif(is_file(public_path() . '/data/preletor/' . $prophet->image))
                        <label class="control-label" for="name">Imagem</label>
                        <img src="/data/preletor/{{ $prophet->image }}" class="img-responsive detail-img-small" alt="{{ $prophet->name or '' }}" title="{{ $prophet->name or '' }}" />
                        @endif
                        <input style="display: none;" value="" id="image" name="image" class="form-control input-lg" type="file">
                        <span id="image-prophet-text" class="help-block">Dimensão mínima de 263 pixels de largura por 280 pixels de altura e formato JPG, JPEG, PNG, ou GIF.</span>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <label class="control-label" for="name">Nome*</label>
                        <input value="{{ $prophet->name or '' }}" type="text" id="name" name="name" placeholder="Nome do preletor" class="form-control input-lg">
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <label class="control-label" for="description">Descrição</label>
                        <textarea id="description" name="description" class="form-control input-lg">
                            {!! $prophet->description or '' !!}
                        </textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <label class="control-label" for="save"></label>
                        <a href="{{ route('controle.prophet.index') }}" class="btn btn-default btn-lg">
                            <i class="fa fa-arrow-left fa-fw"></i> Voltar
                        </a>
                        @if (\App\Permission::verificaPermissao('controle.prophet.update', \Auth::user()->group->id))
                        <button type="submit" id="save" name="save" class="btn btn-primary btn-lg">
                            <i class="fa fa-save fa-fw"></i> Salvar
                        </button>
                        @endif
                        <span id="select-image-prophet" class="btn btn-info btn-lg">
                            <i class="fa fa-image fa-fw"></i> Alterar Imagem
                        </span>
                        @if (\App\Permission::verificaPermissao('controle.prophet.image', \Auth::user()->group->id))
                        <a href="{{ route('controle.prophet.image', $id) }}" class="btn btn-info btn-lg">
                            <i class="fa fa-image fa-fw"></i> Cortar imagem
                        </a>
                        @endif
                        @if(\App\Permission::verificaPermissao('controle.prophet.destroyimage', \Auth::user()->group->id) && is_file(public_path() . '/data/preletor/' . $prophet->image))
                        <a href="{{ route('controle.prophet.destroyimage', $id) }}" class="btn btn-danger btn-lg delete-item" data-toggle="modal" data-target="#deleteItemModal">
                            <i class="fa fa-trash fa-fw"></i> Excluir Imagem
                        </a>
                        @endif
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
@include('includes.modal')
@endsection
@section('scripts')
    <script src="/library/controle/tinymce/js/tinymce/tinymce.min.js"></script>
    <script src="/library/controle/tinymce/js/tinymce/langs/pt_BR.js"></script>
    <script>
        tinymce.init({
            selector: 'textarea',
            language: 'pt_BR'
        });

        $('#select-image-prophet').click(function(){
            $('#image').trigger('click');
        });

        $('#image').change(function(){
            if ($(this).val()) {
                $('#image-prophet-text').html($(this).val());
            } else {
                $('#image-prophet-text').html('Dimensão mínima de 263 pixels de largura por 280 pixels de altura e formato JPG, JPEG, PNG, ou GIF.');
            }
        });
    </script>
@endsection