@extends('layouts.controle')

@section('title', 'Cadastrar preletor')

@section('sidebar')
    @parent

@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Cadastrar preletor</h1>
            </div>
            <!-- /.col-lg-12 -->
            <div class="col-lg-12">
                @include('includes.mensagem')
                <form action="{{ route('controle.prophet.store') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label class="control-label" for="name">Imagem</label>
                            <input required style="display: none;" value="{{ old('image') or '' }}" id="image" name="image" class="form-control input-lg" type="file">
                            <span id="image-prophet-text" class="help-block">Dimensão mínima de 263 pixels de largura por 280 pixels de altura e formato JPG, JPEG, PNG, ou GIF.</span>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label class="control-label" for="name">Nome*</label>
                            <input value="{{  old('name') or '' }}" type="text" id="name" name="name" class="form-control input-lg">
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label class="control-label" for="description">Descrição</label>
                            <textarea id="description" name="description" class="form-control input-lg">
                                {!! old('description') or '' !!}
                            </textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label class="control-label" for="save"></label>
                            <a href="{{ route('controle.prophet.index') }}" class="btn btn-default btn-lg">
                                <i class="fa fa-arrow-left fa-fw"></i> Voltar
                            </a>
                            @if (\App\Permission::verificaPermissao('controle.prophet.store'))
                            <button type="submit" id="save" name="save" class="btn btn-primary btn-lg">
                                <i class="fa fa-save fa-fw"></i> Salvar
                            </button>
                            @endif
                            <span id="select-image-prophet" class="btn btn-info btn-lg">
                                <i class="fa fa-image fa-fw"></i> Escolher Imagem
                            </span>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
@endsection
@section('scripts')
<script src="/library/controle/tinymce/js/tinymce/tinymce.min.js"></script>
<script src="/library/controle/tinymce/js/tinymce/langs/pt_BR.js"></script>
<script>
    tinymce.init({
        selector: 'textarea',
        language: 'pt_BR'
    });

    $('#select-image-prophet').click(function(){
        $('#image').trigger('click');
    });

    $('#image').change(function(){
        if ($(this).val()) {
            $('#image-prophet-text').html($(this).val());
        } else {
            $('#image-prophet-text').html('Dimensão mínima de 263 pixels de largura por 280 pixels de altura e formato JPG, JPEG, PNG, ou GIF.');
        }
    });
</script>
@endsection