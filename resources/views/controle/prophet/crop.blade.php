@extends('layouts.controle')

@section('title', 'Listar preletores')
@section('styles')
<link rel="stylesheet" type="text/css" href="/library/controle/jcrop/css/jquery.Jcrop.css">
@endsection
@section('sidebar')
@parent

@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Cortar imagem</h1>
        </div>
    </div>  
    <div class="row">
        <div class="col-lg-12">
            @if(is_file(public_path() . '/data/preletor/' . $prophet->image))
            <img src="/data/preletor/{{ $prophet->image }}" id="cropbox" class="img-responsive" alt="{{ $prophet->name }}" />
            @endif
        </div>
    </div>
    <div class="row cortar-imagem">
        <div class="col-lg-12">
            <form action="{{ route('controle.prophet.cropimage', $prophet->id) }}" method="post" class="form-horizontal" enctype="multipart/form-data" onsubmit="return checkCoords();">
                <input type="hidden" id="x" name="x" />
                <input type="hidden" id="y" name="y" />
                <input type="hidden" id="w" name="w" />
                <input type="hidden" id="h" name="h" />
                <input type="hidden" id="imageWidth" name="imageWidth" />
                <input type="hidden" id="imageHeight" name="imageHeight" />
                {!! csrf_field() !!}
                <a href="{{ route('controle.prophet.edit', $id) }}" class="btn btn-default btn-lg">
                    <i class="fa fa-arrow-left fa-fw"></i> Voltar
                </a>
                <button type="submit" class="btn btn-lg btn-primary">
                    <i class="fa fa-crop fa-fw"></i> Cortar imagem
                </button>
            </form>
        </div>
    </div>
</div>
@include('includes.modal')
<!-- /.container-fluid -->
@endsection
@section('scripts')
<script src="/library/controle/jcrop/js/jquery.Jcrop.min.js"></script>
<script src="/library/controle/jcrop/js/jquery.color.js"></script>
<script type="text/javascript">
  $(function(){
    $('#cropbox').Jcrop({
        onSelect: updateCoords,
        onChange: updateCoords,
        bgColor: 'black',
        bgOpacity: .4,
        setSelect: [ 263, 280, 100, 100 ],
        aspectRatio: 263 / 280,
        minSize: [ 263, 280],
        maxSize: [ 263 + 1000, 280 + 1000]
    });
  });
  function updateCoords(c)
  {
    $('#x').val(c.x);
    $('#y').val(c.y);
    $('#w').val(c.w);
    $('#h').val(c.h);
    var imgw = $('#cropbox').width();
    var imgh = $('#cropbox').height();
    $('#imageWidth').val(imgw);
    $('#imageHeight').val(imgh);
  };
  function checkCoords()
  {
    if (parseInt($('#w').val())) return true;
    alert('Por favor selecione uma área e clique em Cortar imagem.');
    return false;
  };
</script>
@endsection