@extends('layouts.controle')

@section('title', 'Ver detalhe do preletor')

@section('sidebar')
    @parent

@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{{ $prophet->name or 'Ver detalhe do preletor' }}</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                @include('includes.mensagem')
                <div class="form-group">
                    <div class="col-xs-12 col-md-12 col-sm-10 col-lg-6">
                        <img src="/data/preletor/{{ $prophet->image }}" class="img-responsive" alt="{{ $prophet->name }}" title="{{ $prophet->name }}" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12 col-md-12 col-sm-10 col-lg-10">
                        <label class="control-label">Descrição</label>
                        <div class="form-control input-lg">{!! $prophet->description or '' !!}</div>
                    </div>
                    <div class="col-xs-12 col-md-12 col-sm-6 col-lg-3">
                        <label class="control-label">Criação</label>
                        <div class="form-control input-lg">{{ $prophet->created_at }}</div>
                    </div>
                    <div class="col-xs-12 col-md-12 col-sm-6 col-lg-3">
                        <label class="control-label">Última atualização</label>
                        <div class="form-control input-lg">{{ $prophet->updated_at }}</div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
@endsection