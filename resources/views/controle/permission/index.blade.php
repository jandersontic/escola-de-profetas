@extends('layouts.controle')

@section('title', 'Permissões')

@section('sidebar')
@parent

@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Painel de Controle</h1>
        <p>Selecione o Grupo que deseja definir permissões.</p>
    </div>
    <div class="col-lg-7">
        @include('includes.mensagem')
        <form id="form-group-id" action="{{ route('controle.permission.index') }}" method="POST">
            {!! csrf_field() !!}
            <div class="form-group">
                <label for="grupo">Grupo</label>
                <select id="group_id" name="group_id" class="form-control">
                    <option value="">Selecione</option>
                    @forelse($groups as $group)
                    <option {{ $group->id == $group_id ? 'selected' : '' }} value="{{ $group->id }}">{{ $group->name }}</option>
                    @empty
                    @endforelse
                </select>
            </div>
        </form>
    </div>
    @if ($group_id)
    <div class="col-lg-12">
        <!-- Nav tabs -->
        <ul class="nav nav-pills">
            @forelse($groupTransactions as $key => $groupTransaction)
            <li class="{{ !$key ? 'active' : '' }}"><a href="#{{ $groupTransaction->id }}-pills" data-toggle="tab">{{ $groupTransaction->name }}</a></li>
            @empty
            @endforelse
        </ul>
        <form method="post" action="{{ route('controle.permission.save', $group_id) }}">
            {!! csrf_field() !!}
            <!-- Tab panes -->
            <div class="tab-content">
                @forelse($groupTransactions as $key => $groupTransaction)
                <div class="tab-pane fade {{ !$key ? 'in active' : '' }}" id="{{ $groupTransaction->id }}-pills">
                    <h4>Selecione as permissões para o Módulo {{ $groupTransaction->name }}</h4>
                    @forelse($groupTransaction->transactions()->get() as $transaction)
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" {{ array_key_exists($transaction->id, $permissions) ? $permissions[$transaction->id] : '' }} name="{{ $transaction->name }}" value="{{ $transaction->name }}"> {{ $transaction->action }}
                        </label>
                    </div>
                    @empty
                    @endforelse
                </div>
                @empty
                @endforelse
            </div>
            <div class="form-group">
                <button class="btn btn-success btn-lg" type="submit"><i class="fa fa-save"></i> Salvar</button>
            </div>
        </form>
    </div>
    @endif
</div>
<!-- /.row -->

@endsection
@section('scripts')
<script type="text/javascript">
    $('#form-group-id').change(function(){
       $(this).submit();
    });
</script>
@endsection