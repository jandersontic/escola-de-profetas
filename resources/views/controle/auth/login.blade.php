<!DOCTYPE html>
<html lang="pt-br">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Painel de Controle - Login</title>

    <!-- Bootstrap Core CSS -->
    <link href="/library/controle/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="/library/controle/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="/css/controle/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="/library/controle/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Login</h3>
                    </div>
                    <div class="panel-body">
                        @include('includes.mensagem')
                        <form role="form" method="POST" action="{{ route('controle.auth.logar') }}">
                            {!! csrf_field() !!}
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="E-mail" name="email" type="email" autofocus required value="{{ old('email', '') }}">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Senha" name="password" required type="password">
                                </div>
                                {{--<div class="checkbox">--}}
                                    {{--<label>--}}
                                        {{--<input name="remember" type="checkbox" value="Remember Me">Lembrar-me--}}
                                    {{--</label>--}}
                                {{--</div>--}}
                                <p><a href="{{ route('controle.password.getemail') }}">Esqueceu sua senha?</a></p>
                                <button type="submit" class="btn btn-lg btn-success btn-block">Entrar</button>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="/library/controle/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="/library/controle/bootstrap/dist/js/bootstrap.min.js"></script>

    <script type="text/javascript">
        $(function() {
            $('#close-msg').bind('click', function(){
               $('#body-msg').fadeOut(100);
            });
        });
    </script>    
</body>

</html>