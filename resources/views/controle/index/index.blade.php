@extends('layouts.controle')

@section('title', 'Home')

@section('sidebar')
@parent

@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Painel de Controle</h1>
        @include('includes.mensagem')
    </div>
</div>
@endsection