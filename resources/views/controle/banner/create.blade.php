@extends('layouts.controle')

@section('title', 'Criar um novo banner')

@section('sidebar')
    @parent

@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Criar um novo banner</h1>
            </div>
            <!-- /.col-lg-12 -->
            <div class="col-lg-12">
                @include('includes.mensagem')
                <form action="{{ route('controle.banner.store') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                    {!! csrf_field() !!}

                    <div class="form-group">
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <label class="control-label" for="title">Imagem</label>
                            <input required style="display: none;" value="{{ old('image') or '' }}" id="image" name="image" class="form-control input-lg" type="file">
                            <span id="image-banner-text" class="help-block">Envie imagens nos formatos JPG, JPEG, PNG, ou GIF.</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <label class="control-label" for="title">Título*</label>
                            <textarea id="title" name="title" class="form-control input-lg">
                            {!!  old('title') or '' !!}
                            </textarea>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <label class="control-label" for="description">Descrição</label>
                            <textarea id="description" name="description" class="form-control input-lg">
                                {!! old('description') or '' !!}
                            </textarea>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <label class="control-label" for="link">Link</label>
                            <input value="{{ old('link') or '' }}" id="link" name="link" placeholder="Link do Banner" class="form-control input-lg" type="text">
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                            <label class="control-label" for="order">Ordem</label>
                            <input value="{{ old('order') or '' }}" id="order" name="order" placeholder="Ordem do Banner" class="form-control input-lg" type="number">
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                            <label class="control-label" for="active">Status*</label>
                            <select name="active" id="active" class="form-control input-lg">
                                <option {{ old('active') && old('active') == 1 ? 'selected' : ''  }} value="1">Ativo</option>
                                <option {{ !old('active') || old('active') != 1 ? 'selected' : ''  }} value="0">Inativo</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label class="control-label" for="save"></label>
                            <a href="{{ route('controle.banner.index') }}" class="btn btn-default btn-lg">
                                <i class="fa fa-arrow-left fa-fw"></i> Voltar
                            </a>
                            @if (\App\Permission::verificaPermissao('controle.banner.store'))
                            <button type="submit" id="save" name="save" class="btn btn-primary btn-lg">
                                <i class="fa fa-save fa-fw"></i> Salvar
                            </button>
                            @endif
                            <span id="select-image-banner" class="btn btn-info btn-lg">
                                <i class="fa fa-image fa-fw"></i> Escolher Imagem
                            </span>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
@endsection
@section('scripts')
<script src="/library/controle/tinymce/js/tinymce/tinymce.min.js"></script>
<script src="/library/controle/tinymce/js/tinymce/langs/pt_BR.js"></script>
<script>
    tinymce.init({
        selector: 'textarea',
        language: 'pt_BR'
    });

    $('#select-image-banner').click(function(){
        $('#image').trigger('click');
    });

    $('#image').change(function(){
        if ($(this).val()) {
            $('#image-banner-text').html($(this).val());
        } else {
            $('#image-banner-text').html('Envie imagens nos formatos JPG, JPEG, PNG, ou GIF');
        }
    });
</script>
@endsection