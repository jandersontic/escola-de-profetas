@extends('layouts.controle')

@section('title', 'Listar Banners')

@section('sidebar')
@parent

@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Banners</h1>
        </div>
        <div class="col-lg-12">
            @include('includes.mensagem')
            <a href="{{ route('controle.banner.create') }}" class="btn btn-primary btn-lg right">
                <i class="fa fa-plus fa-fw"></i>Adicionar
            </a>
        </div>
        <!-- /.col-lg-12 -->
        <div class="col-lg-12">
            <div class="table-responsive">
                <table class="table table-condensed sorted_table">
                    <thead>
                        <tr>
                            <th>Ordem</th>
                            <th>Imagem</th>
                            <th>Título</th>
                            <th>Status</th>
                            <th>Opçoes</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($banners as $banner)
                        <tr>
                            <td id="{{ $banner->id }}">{{ $banner->order }}</td>
                            <td>
                                @if(is_file(public_path() . '/data/banner/' . $banner->image))
                                <img src="/data/banner/{{ $banner->image }}" style="max-width: 100px;" class="img-responsive" alt="{{ $banner->title }}" />
                                @endif
                            </td>
                            <td>{!! $banner->title !!}</td>
                            <td>{{ $banner->active ? 'Ativo' : 'Inativo' }}</td>
                            <td>
                                <a href="{{ route('controle.banner.edit', $banner->id) }}" class="btn btn-primary btn-lg">
                                    <i class="fa fa-pencil fa-fw"></i>Editar
                                </a>
                                <a href="{{ route('controle.banner.destroy', $banner->id) }}" class="btn btn-danger btn-lg delete-item" data-toggle="modal" data-target="#deleteItemModal">
                                    <i class="fa fa-trash fa-fw"></i>Excluir
                                </a>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="2">
                                Nenhum registro encontrado!
                            </td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
@include('includes.modal')
<!-- /.container-fluid -->
@endsection
@section('scripts')
<script src="/library/controle/jquery/dist/jquery-sortable-min.js"></script>
<script>
    // Sortable rows
    $('.sorted_table').sortable({
        containerSelector: 'table',
        itemPath: '> tbody',
        itemSelector: 'tr',
        placeholder: '<tr class="placeholder"/>'
    });
</script>
@endsection