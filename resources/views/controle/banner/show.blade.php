@extends('layouts.controle')

@section('title', 'Ver detalhe do banner')

@section('sidebar')
    @parent

@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{{ $banner->title or 'Ver detalhe do banner' }}</h1>
            </div>
            <!-- /.col-lg-12 -->
            <div class="col-lg-12">
                @include('includes.mensagem')
                <div class="form-group">
                    <div class="col-xs-12 col-md-12 col-sm-10 col-lg-6">
                        <img src="data/banner/{{ $banner->image }}" class="img-responsive" alt="{{ $banner->title }}" title="{{ $banner->title }}" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12 col-md-12 col-sm-10 col-lg-10">
                        <label class="control-label">Link</label>
                        <div class="form-control input-lg">{{ $banner->link or '' }}</div>
                    </div>
                    <div class="col-xs-12 col-md-12 col-sm-6 col-lg-3">
                        <label class="control-label">Ordem</label>
                        <div class="form-control input-lg">{{ $banner->order or '' }}</div>
                    </div>
                    <div class="col-xs-12 col-md-12 col-sm-6 col-lg-3">
                        <label class="control-label">Status</label>
                        <div class="form-control input-lg">{{ $banner->active ? 'Ativo' : 'Inativo' }}</div>
                    </div>
                    <div class="col-xs-12 col-md-12 col-sm-6 col-lg-3">
                        <label class="control-label">Criação</label>
                        <div class="form-control input-lg">{{ $banner->created_at }}</div>
                    </div>
                    <div class="col-xs-12 col-md-12 col-sm-6 col-lg-3">
                        <label class="control-label">Última atualização</label>
                        <div class="form-control input-lg">{{ $banner->updated_at }}</div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
@endsection