@extends('layouts.controle')

@section('title', 'Ver detalhe do evento')

@section('sidebar')
    @parent

@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{{ $event->title or 'Ver detalhe do evento' }}</h1>
            </div>
            <!-- /.col-lg-12 -->
            <div class="col-lg-12">
                @include('includes.mensagem')
                <div class="form-group">
                    <div class="col-xs-12 col-md-12 col-sm-12 col-lg-6">
                        <label class="control-label">Título</label>
                        <div class="form-control input-lg">{{ $event->title or '' }}</div>
                    </div>
                    <div class="col-xs-12 col-md-12 col-sm-6 col-lg-3">
                        <label class="control-label">Início</label>
                        <div class="form-control input-lg">{{ $event->begin or '' }}</div>
                    </div>
                    <div class="col-xs-12 col-md-12 col-sm-6 col-lg-3">
                        <label class="control-label">Término</label>
                        <div class="form-control input-lg">{{ $event->end or '' }}</div>
                    </div>
                    <div class="col-xs-12 col-md-12 col-sm-6 col-lg-6">
                        <label class="control-label">Descrição</label>
                        <div class="form-control input-lg">{!! $event->description or '' !!}</div>
                    </div>
                    <div class="col-xs-12 col-md-12 col-sm-6 col-lg-6">
                        <label class="control-label">Endereço</label>
                        <div class="form-control input-lg">{{ $event->address or '' }}</div>
                    </div>
                    <div class="col-xs-12 col-md-12 col-sm-12 col-lg-6">
                        <label class="control-label">Valor individual</label>
                        <div class="form-control input-lg">{{ $event->amount or '' }}</div>
                    </div>
                    <div class="col-xs-12 col-md-12 col-sm-12 col-lg-6">
                        <label class="control-label">Vagas</label>
                        <div class="form-control input-lg">{{ $event->quantity or '' }}</div>
                    </div>
                    <div class="col-xs-12 col-md-12 col-sm-6 col-lg-3">
                        <label class="control-label">Status</label>
                        <div class="form-control input-lg">{{ $event->active ? 'Ativo' : 'Inativo' }}</div>
                    </div>
                    <div class="col-xs-12 col-md-12 col-sm-6 col-lg-3">
                        <label class="control-label">Criação</label>
                        <div class="form-control input-lg">{{ $event->created_at }}</div>
                    </div>
                    <div class="col-xs-12 col-md-12 col-sm-6 col-lg-3">
                        <label class="control-label">Última atualização</label>
                        <div class="form-control input-lg">{{ $event->updated_at }}</div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
@endsection