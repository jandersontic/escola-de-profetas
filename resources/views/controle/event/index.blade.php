@extends('layouts.controle')

@section('title', 'Listar Eventos')

@section('sidebar')
@parent

@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Eventos</h1>
        </div>
        <div class="col-lg-12">
            @include('includes.mensagem')
            <a href="{{ route('controle.event.create') }}" class="btn btn-primary btn-lg right">
                <i class="fa fa-plus fa-fw"></i>Adicionar
            </a>
        </div>
        <!-- /.col-lg-12 -->
        <div class="col-lg-12">
            <div class="table-responsive">
                <table class="table table-condensed sorted_table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Título</th>
                            <th>Início</th>
                            <th>Status</th>
                            <th>Opções</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($events as $event)
                        <tr>
                            <td id="{{ $event->id }}">{{ $event->id }}</td>
                            <td>{{ $event->title }}</td>
                            <td>{{ $event->begin }}</td>
                            <td>{{ $event->active ? 'Ativo' : 'Inativo' }}</td>
                            <td>
                                <a href="{{ route('controle.event.edit', $event->id) }}" class="btn btn-primary btn-lg">
                                    <i class="fa fa-pencil fa-fw"></i>Editar
                                </a>
                                <a href="{{ route('controle.event.destroy', $event->id) }}" class="btn btn-danger btn-lg delete-item" data-toggle="modal" data-target="#deleteItemModal">
                                    <i class="fa fa-trash fa-fw"></i>Excluir
                                </a>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="2">
                                Nenhum registro encontrado!
                            </td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
@include('includes.modal')
<!-- /.container-fluid -->
@endsection
@section('scripts')
@endsection