@extends('layouts.controle')

@section('title', 'Editar Evento')
@section('styles')
<link rel="stylesheet" type="text/css" href="/library/controle/image-picker-master/image-picker/image-picker.css">
@endsection
@section('sidebar')
@parent

@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Editar Evento</h1>
        </div>
        <!-- /.col-lg-12 -->
        <div class="col-lg-12">
            @include('includes.mensagem')
            <form action="{{ route('controle.event.update', $id) }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <div class="form-group">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                        <label class="control-label" for="title">Título*</label>
                        <input value="{{ $event->title or '' }}" id="title" name="title" class="form-control input-lg" required>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                        <label class="control-label" for="begin">Início*</label>
                        <input value="{{ $event->begin or '' }}" id="begin" name="begin" class="form-control input-lg datepicker" type="text" required>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                        <label class="control-label" for="end">Fim*</label>
                        <input value="{{ $event->end or '' }}" id="end" name="end" class="form-control input-lg datepicker" type="text" required>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <label class="control-label" for="description">Descrição</label>
                        <textarea id="description" name="description" class="form-control input-lg">
                            {!! $event->description or '' !!}
                        </textarea>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <label class="control-label" for="address">Endereço*</label>
                        <input value="{{ $event->address or '' }}" id="address" name="address" class="form-control input-lg" type="text" required>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
                        <label class="control-label" for="begin">Valor individual*</label>
                        <input value="{{ $event->amount or '' }}" id="amount" name="amount" class="form-control input-lg" type="text" required>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
                        <label class="control-label" for="end">Vagas</label>
                        <input value="{{ $event->quantity or '' }}" id="quantity" name="quantity" class="form-control input-lg" type="number">
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
                        <label class="control-label" for="active">Status*</label>
                        <select name="active" id="active" class="form-control input-lg">
                            <option {{ $event->active && $event->active == 1 ? 'selected' : ''  }} value="1">Ativo</option>
                            <option {{ !$event->active || $event->active != 1 ? 'selected' : ''  }} value="0">Inativo</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <label class="control-label" for="active">Selecionar preletores</label>
                        @if(count($prophets))
                        <select name="prophets[]" multiple="multiple" class="image-picker show-html">
                            @foreach($prophets as $prophet)
                            @if(is_file(public_path() . '/data/preletor/cut-' . $prophet->image))
                            <option {{ $event->prophets()->where('prophet_id', $prophet->id)->where('event_id', $event->id)->count() ? 'selected' : '' }} data-img-src="/data/preletor/cut-{{ $prophet->image }}" value="{{ $prophet->id }}">{{ $prophet->name }}</option>
                            @elseif(is_file(public_path() . '/data/preletor/' . $prophet->image))
                            <option {{ $event->prophets()->where('prophet_id', $prophet->id)->where('event_id', $event->id)->count() ? 'selected' : '' }} data-img-src="/data/preletor/{{ $prophet->image }}" value="{{ $prophet->id }}">{{ $prophet->name }}</option>
                            @endif
                            @endforeach
                        </select>
                        @else
                        <p>Nenhum preletor cadastrado!</p>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <label class="control-label" for="save"></label>
                        <a href="{{ route('controle.event.index') }}" class="btn btn-default btn-lg">
                            <i class="fa fa-arrow-left fa-fw"></i> Voltar
                        </a>
                        @if (\App\Permission::verificaPermissao('controle.event.update', \Auth::user()->group->id))
                        <button type="submit" id="save" name="save" class="btn btn-primary btn-lg">
                            <i class="fa fa-save fa-fw"></i> Salvar
                        </button>
                        @endif
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
@include('includes.modal')
@endsection
@section('scripts')
<script src="/library/controle/tinymce/js/tinymce/tinymce.min.js"></script>
<script src="/library/controle/tinymce/js/tinymce/langs/pt_BR.js"></script>
<script src="/library/controle/image-picker-master/image-picker/image-picker.min.js"></script>
<script>
    $(document).ready(function(){
        $(".image-picker").imagepicker({
            hide_select : true,
            show_label  : true
        });

        tinymce.init({
            selector: 'textarea',
            language: 'pt_BR',
            plugins: [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools'
              ],
              toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media | forecolor backcolor emoticons'
        });
    });
</script>
@endsection