@extends('layouts.controle')

@section('title', 'Listar Grupos')

@section('sidebar')
@parent

@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Grupos</h1>
        </div>
        <div class="col-lg-12">
            @include('includes.mensagem')
            <a href="{{ route('controle.group.create') }}" class="btn btn-primary right">Adicionar</a>
        </div>
        <!-- /.col-lg-12 -->
        <div class="col-lg-12">
            <div class="table-responsive">
                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Opçoes</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($groups as $group)
                        <tr>
                            <td>{{ $group->name }}</td>
                            <td>
                                <a href="{{ route('controle.group.edit', $group->id) }}" class="btn btn-primary btn-xs"><i class="fa fa-pencil fa-fw"></i>Editar</a>
                                <a href="{{ route('controle.group.destroy', $group->id) }}" class="btn btn-danger btn-xs delete-item" data-toggle="modal" data-target="#deleteItemModal"><i class="fa fa-trash fa-fw"></i>Excluir</a>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="2">
                                Nenhum registro encontrado!
                            </td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
@include('includes.modal')
<!-- /.container-fluid -->
@endsection