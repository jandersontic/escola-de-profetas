@extends('layouts.controle')

@section('title', 'Ver detalhe do grupo')

@section('sidebar')
    @parent

@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Ver detalhe do grupo</h1>
            </div>
            <!-- /.col-lg-12 -->
            <div class="col-lg-12">
                @include('includes.mensagem')
                <div class="form-group">
                    <label class="col-md-1 control-label" for="name">Nome</label>
                    <div class="col-md-4">
                        <div class="form-control input-md">{{ $group->name or '' }}</div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
@endsection