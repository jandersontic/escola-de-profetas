@extends('layouts.controle')

@section('title', 'Criar um novo grupo')

@section('sidebar')
    @parent

@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Criar um novo grupo</h1>
            </div>
            <!-- /.col-lg-12 -->
            <div class="col-lg-12">
                @include('includes.mensagem')
                <form action="{{ route('controle.group.store') }}" class="form-horizontal" method="post">
                    {!! csrf_field() !!}

                    <div class="form-group">
                        <label class="col-md-1 control-label" for="name">Nome</label>
                        <div class="col-md-4">
                            <input value="{{ old('name') or '' }}" id="name" name="name" placeholder="Nome do Grupo" class="form-control input-md" required="" type="text">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label" for="save"></label>
                        <div class="col-md-8">
                            @if (\App\Permission::verificaPermissao('controle.group.store'))
                            <button type="submit" id="save" name="save" class="btn btn-primary">Salvar</button>
                            @endif
                            <a href="{{ route('controle.group.index') }}" class="btn btn-danger">Cancelar</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
@endsection