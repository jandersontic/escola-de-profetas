@extends('layouts.controle')

@section('title', 'Ver detalhe da mensagem')

@section('sidebar')
    @parent

@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{{ $quote->author or 'Ver detalhe da mensagem' }}</h1>
            </div>
            <!-- /.col-lg-12 -->
            <div class="col-lg-12">
                @include('includes.mensagem')
                <div class="form-group">
                    <div class="col-xs-12 col-md-6 col-sm-3 col-lg-3">
                        <label class="control-label">Autor</label>
                        <div class="form-control input-lg">{{ $quote->author or '' }}</div>
                    </div>
                    <div class="col-xs-12 col-md-6 col-sm-3 col-lg-3">
                        <label class="control-label">Status</label>
                        <div class="form-control input-lg">{{ $quote->active ? 'Ativo' : 'Inativo' }}</div>
                    </div>
                    <div class="col-xs-12 col-md-6 col-sm-3 col-lg-3">
                        <label class="control-label">Criação</label>
                        <div class="form-control input-lg">{{ $quote->created_at }}</div>
                    </div>
                    <div class="col-xs-12 col-md-6 col-sm-3 col-lg-3">
                        <label class="control-label">Última atualização</label>
                        <div class="form-control input-lg">{{ $quote->updated_at }}</div>
                    </div>
                    <div class="col-xs-12 col-md-12 col-sm-12 col-lg-12">
                        <label class="control-label">Citação</label>
                        <textarea readonly class="form-control uneditable-textarea">
                            {!! $quote->citation or '' !!}
                        </textarea>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
@endsection