@extends('layouts.controle')

@section('title', 'Editar Mensagem')

@section('sidebar')
@parent

@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Editar Mensagem</h1>
        </div>
        <!-- /.col-lg-12 -->
        <div class="col-lg-12">
            @include('includes.mensagem')
            <form action="{{ route('controle.quote.update', $id) }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <div class="form-group">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                        <label class="control-label" for="author">Autor*</label>
                        <input value="{{ $quote->author or '' }}" id="author" name="author" class="form-control input-lg" required>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                        <label class="control-label" for="order">Ordem</label>
                        <input value="{{ $quote->order or '' }}" id="order" name="order" class="form-control input-lg" type="number">
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                        <label class="control-label" for="active">Status*</label>
                        <select name="active" id="active" class="form-control input-lg">
                            <option {{ $quote->active && $quote->active == 1 ? 'selected' : ''  }} value="1">Ativo</option>
                            <option {{ !$quote->active || $quote->active != 1 ? 'selected' : ''  }} value="0">Inativo</option>
                        </select>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <label class="control-label" for="citation">Citação</label>
                            <textarea id="citation" name="citation" class="form-control input-lg">
                                {!! $quote->citation or '' !!}
                            </textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <label class="control-label" for="save"></label>
                        <a href="{{ route('controle.quote.index') }}" class="btn btn-default btn-lg">
                            <i class="fa fa-arrow-left fa-fw"></i> Voltar
                        </a>
                        @if (\App\Permission::verificaPermissao('controle.quote.update', \Auth::user()->group->id))
                        <button type="submit" id="save" name="save" class="btn btn-primary btn-lg">
                            <i class="fa fa-save fa-fw"></i> Salvar
                        </button>
                        @endif
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
@include('includes.modal')
@endsection
@section('scripts')
<script src="/library/controle/tinymce/js/tinymce/tinymce.min.js"></script>
<script src="/library/controle/tinymce/js/tinymce/langs/pt_BR.js"></script>
<script>
    $(document).ready(function(){
        tinymce.init({
            selector: 'textarea',
            language: 'pt_BR',
            plugins: [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools'
              ],
              toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media | forecolor backcolor emoticons'
        });
    });
</script>
@endsection