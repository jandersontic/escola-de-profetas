<!DOCTYPE html>
<html lang="pt-br" ng-app="webApp">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Escola de Profetas Belém - Não somos desse mundo.">
    <meta name="author" content="Janderson Silva">
    <title>@yield('title') - Painel de Controle</title>

    <link rel="stylesheet" type="text/css" href="/library/controle/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/library/controle/metisMenu/dist/metisMenu.min.css">
    <link rel="stylesheet" type="text/css" href="/css/controle/timeline.css">
    <link rel="stylesheet" type="text/css" href="/library/controle/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/library/controle/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" type="text/css" href="/library/controle/colorbox/example1/colorbox.css">
    <link rel="stylesheet" type="text/css" href="/css/controle/sb-admin-2.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    @section('styles')
    @show
</head>
<body>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ route('controle.index.index') }}">Escola de Profetas Belém</a>
            </div>
            <!-- /.navbar-header -->
            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        Olá, {{ Auth::check() ? current(explode(" ", Auth::User()->name)) : "" }} <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li>
                            <a href="{{ route('controle.user.getprofile') }}"><i class="fa fa-gear fa-fw"></i> Meu Perfil</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="{{ route('controle.auth.logout') }}"><i class="fa fa-sign-out fa-fw"></i> Sair</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Pesquisar...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        <li>
                            <a href="{{ route('controle.index.index') }}"><i class="fa fa-home fa-fw"></i> Início</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-sitemap fa-fw"></i> Site<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{ route('controle.banner.index') }}">Banners</a>
                                </li>
                                <li>
                                    <a href="{{ route('controle.event.index') }}">Eventos</a>
                                </li>
                                <li>
                                    <a href="{{ route('controle.prophet.index') }}">Preletores</a>
                                </li>
                                <li class="disabled">
                                    <a href="">Inscrições</a>
                                </li>
                                <li class="disabled">
                                    <a href="">Galeria</a>
                                </li>
                                <li class="disabled">
                                    <a href="">Programação</a>
                                </li>
                                <li>
                                    <a href="{{ route('controle.quote.index') }}">Mensagens</a>
                                </li>
                                <li class="disabled">
                                    <a href="">Notícias</a>
                                </li>
                                <li class="disabled">
                                    <a href="">Contato</a>
                                </li>
                                <li class="disabled">
                                    <a href="">Como Chegar</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        {{--<li>--}}
                            {{--<a href="#"><i class="fa fa-money fa-fw"></i> Caixa</a>--}}
                        {{--</li>--}}
                        <li>
                            <a href="#"><i class="fa fa-gear fa-fw"></i> Configuração<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{ route('controle.group.index') }}">Grupos</a>
                                </li>
                                <li>
                                    <a href="{{ route('controle.user.index') }}">Usuários</a>
                                </li>
                                <li>
                                    <a href="{{ route('controle.permission.index') }}">Permissões</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="{{ route('controle.auth.logout') }}"><i class="fa fa-sign-out fa-fw"></i> Sair</a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
        <div id="page-wrapper">
           @yield('content')
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    {{--
    <!-- Angular JS 1.4.8 -->
    <script src="/library/controle/angular/angular.min.js"></script>
    <script src="/library/controle/angular/angular-animate.min.js"></script>
    <script src="/library/controle/angular/angular-sanitize.min.js"></script>
    <script src="/library/controle/angular/ngMask.min.js"></script>
    <script src="/js/controle/app.js"></script>
    --}}
    <!-- jQuery -->
    <script src="/library/controle/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="/library/controle/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src="/library/controle/metisMenu/dist/metisMenu.min.js"></script>
    <!-- Maskedinput -->
    <script src="/library/controle/maskedinput/jquery.maskedinput.min.js"></script>
    <!-- Datepicker -->
    <script src="/library/controle/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="/library/controle/bootstrap-datepicker/dist/locales/bootstrap-datepicker.pt-BR.min.js"></script>
    <!-- Colorbox -->
    <script src="/library/controle/colorbox/jquery.colorbox-min.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="/js/controle/sb-admin-2.js"></script>
    @section('scripts')
    @show
</body>
</html>