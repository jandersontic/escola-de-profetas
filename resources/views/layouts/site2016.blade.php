<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Escola de Profetas Belém 2016 - Não somos desse mundo. 21 a 24 de Abril em Belém do Pará.">
        <meta name="keywords" content="Escola de Profetas Belém, 2016, Escola, Profetas, profética Belém, Pará, Deus, Jesus, Inscrições, Preletores">
        <meta name="author" content="Janderson Silva">
        <title>@yield('title')</title>
        <!-- Favicons -->
        <link rel="shortcut icon" href="img/site/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/site/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/site/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/site/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="img/site/apple-touch-icon-57-precomposed.png">
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="js/site/html5shiv.js"></script>
        <script src="js/site/respond.min.js"></script>
        <![endif]-->

        <!-- CSS libraries and styles -->
        <link rel="stylesheet" href="library/site/bower_components/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="library/site/bower_components/fontawesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="library/site/bower_components/flexslider/flexslider.css">
        <link rel="stylesheet" href="library/site/bower_components/smoothdivscroll/css/smoothDivScroll.css">
        <link rel="stylesheet" href="css/site/pnotify.custom.min.css">
        <link rel="stylesheet" href="css/site/styles.css">
        <link rel="stylesheet" href="css/site/retina.css">
        <link rel="stylesheet" href="css/site/small.css" media="screen and (min-width: 768px)">

        <!-- LESS styles -->
        <!--<link rel="stylesheet/css" href="/css/site/style2016.css">-->
        <link rel="stylesheet/less" href="css/site/less.less">
        <!--<link rel="stylesheet/css" href="/css/site/less.css">-->
        <link rel="stylesheet/less" href="css/site/colors/darkgoldenrod.less">
        <!--<link rel="stylesheet/css" href="/css/site/colors/darkgoldenrod.css">-->

        @section('styles')

        @show

    </head>
    <body>
        <!-- Hero -->
        <!--<div id="hero">-->
        <!--<img id="fundo-principal" src="../assets/fundo.jpg" class="image" style="max-width: 55%; max-height: 100%; margin: 0 auto;">-->
        <!-- <div id="YTPlayer" class="player" data-property="{videoURL:'https://www.youtube.com/watch?v=zG-ANI4lA6A',containment:'#hero',optimizeDisplay:true,showControls:false,addRaster:false,autoPlay:true, mute:false, startAt:0, opacity:0.9, stopMovieOnBlur: true, loop: false}"></div> -->
        <!-- <div class="overlay"></div> -->
        <!--div class="content">
            <a href="#about-us" class="button button-inversed button-large">Escola de Profetas Belém</a>
        </div>
        <div class="content">
            <a href="#about-us" class="button button-inversed button-large">Saiba mais</a>
        </div>
        <div class="content">
            <a href="#about-us" class="button button-inversed button-large">Saiba mais</a>
        </div-->
        <!-- video audio icon -->
        <!-- <a href="#" id="video-volume" class="hero-scroll-arrow"><span>Volume</span><i class="fa fa-volume-down"></i></a> -->
        <!--</div>-->

        <!-- Navigation -->
        <nav class="navbar" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="active"><a href="#inicio">Início</a></li>
                        <li><a href="#about-us">Sobre</a></li>
                        <li><a href="#ofertar">Ofertar</a></li>
                        <li><a href="#our-team">Preletores</a></li>
                        <li><a href="#pricing">Inscrições</a></li>
                        <li><a href="/galeria/">Galeria</a></li>
                        <li><a href="#blog">Programação</a></li>
                        <li><a href="#contacts">Contato</a></li>
                        <li><a href="#map">Como Chegar</a></li>
                        <li><a href="{{ route('controle.auth.login') }}">Entrar</a></li>
                    </ul>
                </div>
            </div>
        </nav>

        <!-- Sections -->
        @yield('content')

        <!-- Footer -->
        <footer>
            <div class="logo inversed">Escola de Profetas Belém</div>
            <div class="space"></div>
            <ul class="social">
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a target="_blank" href="https://twitter.com/escoladep"><i class="fa fa-twitter"></i></a></li>
                <!--li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                <li><a href="#"><i class="fa fa-youtube-play"></i></a></li>
                <li><a href="#"><i class="fa fa-instagram"></i></a></li-->
            </ul>
            <div class="space"></div>
            <div class="copyright">&copy; 2014 - <?=date("Y");?> Todos os direitos reservados. Feito por <a href="http://www.jandersonsilva.com/" target="_blank">Janderson Silva</a></div>
        </footer>

        <!-- Popup -->
        <div id="popup" class="popup"></div>

        <!-- Back to top button -->
        <!--<a href="#" class="button button-to-top"><i class="fa fa-chevron-up"></i></a>-->

        <!-- Hidden iframe for contact forms -->
        <iframe name="hidden" class="hide" src="about:blank"></iframe>

        <!-- JS libraries and scripts -->
        <script type="text/javascript" src="js/site/less-1.6.1.min.js"></script>
        <script type="text/javascript" src="js/site/jquery-1.11.0.min.js"></script>
        <script type="text/javascript" src="library/site/bower_components/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="library/site/bower_components/flexslider/jquery.flexslider-min.js"></script>
        <script type="text/javascript" src="library/site/bower_components/smoothdivscroll/js/jquery-ui-1.8.23.custom.min.js"></script>
        <script type="text/javascript" src="library/site/bower_components/smoothdivscroll/js/jquery.mousewheel.min.js"></script>
        <script type="text/javascript" src="library/site/bower_components/smoothdivscroll/js/jquery.kinetic.min.js"></script>
        <script type="text/javascript" src="library/site/bower_components/smoothdivscroll/js/jquery.smoothdivscroll-1.3-min.js"></script>
        <!--<script type="text/javascript" src="js/site/jquery.imgpreload.min.js"></script>-->
        <script type="text/javascript" src="js/site/jquery.parallax.js"></script>
        <script type="text/javascript" src="js/site/jquery.scrollTo-1.4.3.1-min.js"></script>
        <script type="text/javascript" src="js/site/jquery.bez.min.js"></script>
        <script type="text/javascript" src="js/site/isotope.pkgd.min.js"></script>
        <script type="text/javascript" src="js/site/SmoothScroll.min.js"></script>
        <script type="text/javascript" src="js/site/retina-1.1.0.min.js"></script>
        <script type="text/javascript" src="js/site/jquery.livequery.js"></script>
        <script type="text/javascript" src="js/site/pnotify.custom.min.js"></script>
        <script type="text/javascript" src="js/site/jquery.mb.YTPlayer.min.js"></script>
        <script type="text/javascript" src="js/site/scripts.js"></script>

        <!-- Initiate scroll effects -->
        <!--<script type="text/javascript" src="js/site/init-scroll.js"></script>-->
        <!-- Teste de Video HTML5 Multibackground -->
        <!--script type="text/javascript" src="http://preview.tonybogdanov.com/project/savannah/js/jquery.multibackground.min.js"></script-->
        <!-- Google Analytics -->
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-57752046-1', 'auto');
            ga('send', 'pageview');

        </script>
    
    @section('scripts')
        
    @show

    </body>
</html>