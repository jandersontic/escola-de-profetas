<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Escola de Profetas Belém 2016 - Não somos desse mundo. 21 a 24 de Abril em Belém do Pará.">
        <meta name="author" content="Janderson Silva">
        <title>@yield('title')</title>
        <!-- core CSS -->
        <link href="css/site/2017/bootstrap.min.css" rel="stylesheet">
        <link href="css/site/2017/font-awesome.min.css" rel="stylesheet">
        <link href="css/site/2017/animate.min.css" rel="stylesheet">
        <link href="css/site/2017/owl.carousel.css" rel="stylesheet">
        <link href="css/site/2017/owl.transitions.css" rel="stylesheet">
        <link href="css/site/2017/prettyPhoto.css" rel="stylesheet">
        <link href="css/site/2017/main.css" rel="stylesheet">
        <link href="css/site/2017/responsive.css" rel="stylesheet">
        <!--[if lt IE 9]>
        <script src="js/site/2017/html5shiv.js"></script>
        <script src="js/site/2017/respond.min.js"></script>
        <![endif]-->       
        <link rel="shortcut icon" href="img/site/2017/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/site/2017/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/site/2017/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/site/2017/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="img/site/2017/ico/apple-touch-icon-57-precomposed.png">    

        @section('styles')

        @show

    </head>
    <body id="home" class="homepage">
        
        <header id="header">
            <nav id="main-menu" class="navbar navbar-default navbar-fixed-top" role="banner">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="{{ route('site.index.index') }}"><img src="img/site/2017/logo.png" alt="logo"></a>
                    </div>
                    
                    <div class="collapse navbar-collapse navbar-right">
                        <ul class="nav navbar-nav">
                            <li class="scroll active"><a href="#home">Início</a></li>
                            <li class="scroll"><a href="#sobre">Sobre</a></li>
                            <li class="scroll"><a href="#cadastrar">Cadastre-se</a></li>
                            <li class="scroll"><a href="#preletores">Preletores</a></li>
                            {{--<li class="scroll"><a href="#services">Services</a></li>--}}
                            {{--<li class="scroll"><a href="#portfolio">Portfolio</a></li>--}}
                            <li class="scroll"><a href="#testimonial">Mensagens</a></li>
                            {{--<li class="scroll"><a href="#pricing">Pricing</a></li>--}}
                            {{--<li class="scroll"><a href="#blog">Blog</a></li> --}}
                            <li class="scroll"><a href="#contact">Contato</a></li>
                            <li><a href="{{ route('controle.index.index') }}">Entrar</a></li>
                        </ul>
                    </div>
                </div><!--/.container-->
            </nav><!--/nav-->
        </header><!--/header-->
        {{--
        <!-- Navigation -->
        <nav class="navbar" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="active"><a href="#inicio">Início</a></li>
                        <li><a href="#about-us">Sobre</a></li>
                        <li><a href="#ofertar">Ofertar</a></li>
                        <li><a href="#our-team">Preletores</a></li>
                        <li><a href="#pricing">Inscrições</a></li>
                        <li><a href="/galeria/">Galeria</a></li>
                        <li><a href="#blog">Programação</a></li>
                        <li><a href="#contacts">Contato</a></li>
                        <li><a href="#map">Como Chegar</a></li>
                        <li><a href="{{ route('controle.auth.login') }}">Entrar</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        --}}
        <!-- Sections -->
        @yield('content')

        <!-- Footer -->
        <footer id="footer">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        &copy; 2014 - {{ date("Y") }} Escola de Profetas Belém. Feito por <a href="https://br.linkedin.com/in/janderson-silva-8a999962" target="_blank">Janderson Silva</a>
                    </div>
                    <div class="col-sm-6">
                        <ul class="social-icons">
                            {{--<li><a href="#"><i class="fa fa-facebook"></i></a></li>--}}
                            <li><a href="https://twitter.com/escoladep" target="_blank"><i class="fa fa-twitter"></i></a></li>
                            {{--<li><a href="#"><i class="fa fa-google-plus"></i></a></li>--}}
                            {{--<li><a href="#"><i class="fa fa-pinterest"></i></a></li>--}}
                            {{--<li><a href="#"><i class="fa fa-dribbble"></i></a></li>--}}
                            {{--<li><a href="#"><i class="fa fa-behance"></i></a></li>--}}
                            {{--<li><a href="#"><i class="fa fa-flickr"></i></a></li>--}}
                            {{--<li><a href="#"><i class="fa fa-youtube"></i></a></li>--}}
                            {{--<li><a href="#"><i class="fa fa-linkedin"></i></a></li>--}}
                            {{--<li><a href="#"><i class="fa fa-github"></i></a></li>--}}
                        </ul>
                    </div>
                </div>
            </div>
        </footer><!--/#footer-->

        <script src="js/site/2017/jquery.js"></script>
        <script src="js/site/2017/bootstrap.min.js"></script>
        <script src="http://maps.google.com/maps/api/js"></script>
        <script src="js/site/2017/owl.carousel.min.js"></script>
        <script src="js/site/2017/mousescroll.js"></script>
        <script src="js/site/2017/smoothscroll.js"></script>
        <script src="js/site/2017/jquery.prettyPhoto.js"></script>
        <script src="js/site/2017/jquery.isotope.min.js"></script>
        <script src="js/site/2017/jquery.inview.min.js"></script>
        <script src="js/site/2017/wow.min.js"></script>
        <script src="js/site/2017/main.js"></script>

        <!-- Google Analytics -->
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-57752046-1', 'auto');
            ga('send', 'pageview');

        </script>
    
    @section('scripts')
        
    @show

    </body>
</html>