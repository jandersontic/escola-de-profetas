@extends('layouts.site')

@section('title', 'Galeria - Escola de Profetas Belém')

@section('content')
    <section id="portfolio">
        <div class="container">
            <div class="section-header">
                <h2 class="section-title text-center wow fadeInDown">Galeria</h2>
                <p class="text-center wow fadeInDown">Fique por dentro de tudo que aconteceu nas últimas edições da Escola de Profetas Belém</p>
            </div>

            <div class="text-center">
                <ul class="portfolio-filter">
                @forelse($events as $event)
                    <li><a href="#" data-filter=".{{ $event->id }}">{{ $event->getAno($event->begin) }}</a></li>
                @empty
                @endforelse    
                    <li><a class="active" href="#" data-filter="*">Todas as edições</a></li>
                </ul><!--/#portfolio-filter-->
            </div>
            <div class="portfolio-items">
            @forelse($events as $event)
                @forelse($event->midias as $midia)
                <div class="portfolio-item {{ $event->id }}">
                    <div class="portfolio-item-inner">
                        <a class="preview" href="{{ '/data/galeria/' . $midia->image }}" rel="prettyPhoto">
                            @if(is_file(public_path() . '/data/galeria/cut-' . $midia->image))
                            <img src="/data/galeria/cut-{{ $midia->image }}" class="img-responsive" alt="{{ $midia->name }}" />
                            @elseif(is_file(public_path() . '/data/galeria/' . $midia->image))
                            <img src="/data/galeria/{{ $midia->image }}" class="img-responsive" alt="{{ $midia->name }}" />
                            @endif
                        </a>
                        @if(!empty($midia->name) || !empty($midia->description))
                        <div class="portfolio-info">
                            <h3>
                                <a class="preview" href="{{ '/data/galeria/' . $midia->image }}" rel="prettyPhoto">
                                    {{ $midia->name }}
                                </a>
                            </h3>
                            <a class="preview" href="{{ '/data/galeria/' . $midia->image }}" rel="prettyPhoto">
                                {{ $midia->description }}
                            </a>
                        </div>
                        @endif
                    </div>
                </div><!--/.portfolio-item-->
                @empty
                @endforelse
            @empty
            @endforelse
            </div>
        </div><!--/.container-->
    </section><!--/#portfolio-->
   
    <section id="cta" class="wow fadeIn">
        <div class="container">
            <div class="row">
                <div class="col-sm-9">
                    <h2>Faça parte deste ministério</h2>
                    <p>Colabore em oração, voluntariado ou com uma oferta de amor. "Cada um contribua segundo propôs no seu coração; não com tristeza, ou por necessidade; porque Deus ama ao que dá com alegria". <small>2 Coríntios 9:7</small></p>
                </div>
                <div class="col-sm-3 text-right">
                    {{--<a class="btn btn-primary btn-lg" href="#">Download Now!</a>--}}
                            <!-- INICIO FORMULARIO BOTAO PAGSEGURO -->
                    <form action="https://pagseguro.uol.com.br/checkout/v2/donation.html" method="post">
                        <!-- NÃO EDITE OS COMANDOS DAS LINHAS ABAIXO -->
                        <input type="hidden" name="currency" value="BRL" />
                        <input type="hidden" name="receiverEmail" value="escoladeprofetasbelem@gmail.com" />
                        <input type="image" style="display: none;" src="https://p.simg.uol.com.br/out/pagseguro/i/botoes/doacoes/209x48-doar-assina.gif" name="submit" alt="Pague com PagSeguro - é rápido, grátis e seguro!" />
                        <button type="submit" class="btn btn-primary btn-lg">Desejo ofertar</button>
                    </form>
                    <!-- FINAL FORMULARIO BOTAO PAGSEGURO -->
                </div>
            </div>
        </div>
    </section><!--/#cta-->

    <section id="contact">
        <div id="google-map" style="height:650px" data-latitude="-1.3394681" data-longitude="-48.2914038"></div>
        <div class="container-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4 col-sm-offset-8">
                        <div class="contact-form">
                            <h3>Contato</h3>

                            <address>
                                <strong>Rodovia Augusto Montenegro, Km 3.5</strong><br>
                                Em frente ao Residencial Natália Lins<br>
                                Mangueirão, Belém, PA<br>
                                <abbr title="Phone">Fone:</abbr> (91) 3279-0211 / (91) 98295-9495 / (91) 98032-6828<br>
                                E-mail: <a href="mailto:contato&#64;escoladeprofetasbelem.com.br">contato&#64;escoladeprofetasbelem.com.br</a>
                            </address>

                            <form id="main-contact-form" name="contact-form" method="post" action="{{ route('site.index.contato') }}">
                                {!! csrf_field() !!}
                                <div class="form-group">
                                    <input type="text" name="name" class="form-control" placeholder="Nome" required>
                                </div>
                                <div class="form-group">
                                    <input type="email" name="email" class="form-control" placeholder="Email" required>
                                </div>
                                <div class="form-group">
                                    <input type="text" name="subject" class="form-control" placeholder="Assunto" required>
                                </div>
                                <div class="form-group">
                                    <textarea name="bodyMessage" class="form-control" rows="8" placeholder="Menssagem" required></textarea>
                                </div>
                                <button type="submit" class="btn btn-primary">Enviar Menssagem</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!--/#bottom-->
@endsection
@section('scripts')
@endsection