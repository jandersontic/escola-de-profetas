@if (Session::has('msg'))
<div id="body-msg" class="alert alert-{{ Session::get('errors') ? 'danger' : 'success' }}">
    <i id="close-msg" class="close">x</i>
    <p>{!! Session::pull('msg')  !!}</p>
</div>
@endif