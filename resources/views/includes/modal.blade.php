<!-- Confirm Delete Modal -->
<div class="modal fade" id="deleteItemModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Excluir registro!</h4>
      </div>
      <div class="modal-body">
          <p>Deseja realmente <b>excluir</b> o registro?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-lg" data-dismiss="modal"><i class="fa fa-arrow-left fa-fw"></i>Cancelar</button>
        <a class="btn btn-danger btn-lg btn-delete-item"><i class="fa fa-trash fa-fw"></i>Excluir</a>
      </div>
    </div>
  </div>
</div>