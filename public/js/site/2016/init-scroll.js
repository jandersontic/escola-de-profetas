(function() {
    "use strict";

    // Add "animated-out" class to elements with data-animated attribute
    $('[data-animated]').livequery(function() {
        $(this).addClass('animated-out');
    });

    // Switch "animated-out" class to "animated-in" when scrolling
    $(window).scroll(function() {
        var scroll      = $(window).scrollTop();
        var height      = $(window).height();
        $('.animated-out[data-animated]').each(function() {
            var $this   = $(this);
            if(scroll + height * 2 / 3 >= $this.offset().top) {
                var delay   = parseInt($this.attr('data-animated'));
                if(isNaN(delay) || 0 === delay) {
                    $this.removeClass('animated-out').addClass('animated-in');
                } else {
                    setTimeout(function() {
                        $this.removeClass('animated-out').addClass('animated-in');
                    }, delay);
                }
            }
        });
    });
})();