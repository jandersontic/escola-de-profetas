(function($) {
    "use strict";

    // Window resize end event
    $(window).bind('resize', function() {
        var $this   = $(this);
        clearTimeout($this.data('resize-start'));
        $this.data('resize-start', setTimeout(function() {
            $this.triggerHandler('resize-end');
        }, 200));
    });
    
    // Buttons
    $('.button').livequery('mouseover', function() {
        var $this   = $(this);
        if($this.is('.button-inversed')) {
            $this.css('background-position', ($this.outerWidth() - 1004 + Math.ceil(Math.sin(15 * Math.PI / 180) * $this.height() / Math.sin(75 * Math.PI / 180))) + 'px 0');
        } else {
            $this.css('background-position', ($this.outerWidth() - 34 + Math.ceil(Math.sin(15 * Math.PI / 180) * $this.height() / Math.sin(75 * Math.PI / 180))) + 'px 0');
        }
    }).livequery('mouseout', function() {
        var $this   = $(this);
        if($this.is('.button-inversed')) {
            $this.css('background-position', '-1000px 0');
        } else {
            $this.css('background-position', '-30px 0');
        }
    });

    // Fixed navigation
    $(window).bind('scroll', function() {
        var $this   = $(this);
        var $body   = $('body');
        var offset  = 0 === $('#hero').length ? 0 : $('#hero').height();
        if($this.scrollTop() >= offset) {
            $body.addClass('nav-fixed');
        } else {
            $body.removeClass('nav-fixed');
        }
    });

    // Native placeholder support
    if(!('placeholder' in document.createElement('input'))) {
        $('[placeholder]').livequery(function() {
            var $this   = $(this);
            if(0 === $this.val().length) {
                $this.val($this.attr('placeholder'));
            }
        }).livequery('focus', function() {
            var $this   = $(this);
            if($this.val() == $this.attr('placeholder')) {
                $this.val('');
            }
        }).livequery('blur', function() {
            var $this   = $(this);
            if($this.val() == '') {
                $this.val($this.attr('placeholder'));
            }
        });
    }
    
    // Popup window
    $('[href^="#!"]').livequery('click', function(e) {
        e.preventDefault();
        var $this   = $(this);
        var $body   = $('body');
        var $popup  = $('#popup');
        var url     = $this.attr('href').substring(2);
        $popup.load(url, function() {
            $body.addClass('popup-open');
            $popup.find('.exit').bind('click', function(e) {
                e.preventDefault();
                $body.removeClass('popup-open');
                $popup.html('');
            });
        });
    });

    // Smooth section scrolling
    $('[href^="#"]:not([href^="#!"])').livequery('click', function(e) {
        e.preventDefault();
        var $this   = $(this);
        var $target = $($this.attr('href'));
        var offset  = $('.navbar').height();
        var speed   = isNaN(parseInt($('body').attr('data-scroll-speed'))) ? 50 : parseInt($('body').attr('data-scroll-speed'));
        if(0 < $target.length) {
            $.scrollTo.window().queue([]).stop();
            $.scrollTo({left: 0, top: Math.max(0, $target.offset().top - offset)}, {duration: speed, easing: $.bez([0.13, 0.71, 0.30, 0.94])});
        }
    });

    // Hero slider
//    if(0 < $('#hero .content').length) {
//        var slide = function() {
//            var $cur    = $('#hero .content.display.visual');
//            if(0 === $cur.length) {
//                $cur    = $('#hero .content:eq(0)');
//                $cur.addClass('display');
//                setTimeout(function() { $cur.addClass('visual'); }, 50);
//            } else {
//                var $next   = $cur.next('.content');
//                if(0 === $next.length) {
//                    $next   = $('#hero .content:eq(0)');
//                }
//                $cur.removeClass('visual').addClass('visual-end');
//                setTimeout(function() { $cur.removeClass('display').removeClass('visual-end'); }, 1000);
//                $next.addClass('display');
//                setTimeout(function() { $next.addClass('visual'); }, 50);
//            }
//        };
//        var duration = isNaN(parseInt($('body').attr('data-hero-duration'))) ? 5000 : Math.abs(parseInt($('body').attr('data-hero-duration')));
//        setInterval(slide, duration);
//        slide();
//    }

    
    $('.flexslider.banner-principal').flexslider({
        slideshowSpeed: 7000
    });
    
    // Testimonial slider
    $('.flexslider.testimonial-slider').flexslider({
        animation:  "slide",
        prevText:   "",
        nextText:   "",
        slideshowSpeed: 7000,
        randomize: true,
        pausePlay: true,
        controlNav: false,
        slideshow:  true
    });

    // Portfolio isotope
    $('.portfolio').isotope({
        itemSelector: '.item',
        layoutMode: 'masonry',
        masonry: {
            resizable: false,
            columnWidth: 210,
            gutter: 30
        }
    });
    $(window).bind('resize-end', function() {
        var $port   = $('.portfolio');
        var columns = Math.floor($port.parent().width() / 240);
        var target  = (210 * columns) + (30 * (columns - 1));
        $port.width(target);
        setTimeout(function() {
            $port.isotope('layout');
        }, 200);
    });
    
    // Portfolio filtering
    $('.portfolio-filter a').livequery('click', function(e) {
        e.preventDefault();
        var $this   = $(this);
        $this.parent('li').parent('ul').find('li.active').removeClass('active');
        $this.parent('li').addClass('active');
        $('.portfolio').isotope({filter: $this.attr('data-filter')});
    });

    // Blog isotope
    $('.blog').isotope({
        itemSelector: '.blog-item',
        layoutMode: 'masonry',
        masonry: {
            resizable: false,
            columnWidth: 262,
            gutter: 30
        }
    });
    $(window).bind('resize-end', function() {
        var $port   = $('.blog');
        var columns = Math.floor(($port.parent().width() + 30) / 292);
        var target  = (262 * columns) + (30 * (columns - 1));
        $port.width(target);
        setTimeout(function() {
            $port.isotope('layout');
        }, 200);
    });

    // Scroll spy (this needs to be after portfolio bindings)
    $(window).bind('resize', function() {
        $('body').scrollspy({target: '.navbar-collapse', offset: $('.navbar').height() + 1});
    }).triggerHandler('resize');

    // Animate count elements
    $('.count').livequery(function() {
        var $this   = $(this);
        $this.data('target', parseInt($this.html()));
        $this.data('counted', false);
        $this.html('0');
    });
    $(window).bind('scroll', function() {
        var speed   = isNaN(parseInt($('body').attr('data-count-speed'))) ? 5000 : parseInt($('body').attr('data-count-speed'));
        $('.count').each(function() {
            var $this   = $(this);
            if(!$this.data('counted') && $(window).scrollTop() + $(window).height() >= $this.offset().top) {
                $this.data('counted', true);
                $this.animate({dummy: 1}, {
                    duration: speed,
                    easing:   $.bez([0, 0, 0, 1]),
                    step:     function(now) {
                        var $this  = $(this);
                        $this.html(Math.round($this.data('target') * now));
                    }
                });
            }
        });
    }).triggerHandler('scroll');

    // Clients scroll
    $('#clients').smoothDivScroll({
        hotSpotScrolling: false,
        touchScrolling: true,
        manualContinuousScrolling: false,
        mousewheelScrolling: false
    });

    // Back to top button
    $('.button-to-top').bind('click', function(e) {
        e.preventDefault();
        var speed   = isNaN(parseInt($('body').attr('data-scroll-speed'))) ? 50 : parseInt($('body').attr('data-scroll-speed'));
        if($('body').hasClass('popup-open')) {
            $('#popup').scrollTo({left: 0, top: 0}, {duration: speed, easing: $.bez([0.13, 0.71, 0.30, 0.94])});
        } else {
            $(window).scrollTo({left: 0, top: 0}, {duration: speed, easing: $.bez([0.13, 0.71, 0.30, 0.94])});
        }
    });
    
    $(window).scroll(function() {
        if($(window).scrollTop() > 10) {
            $('.button-to-top').addClass('show');
        } else {
            $('.button-to-top').removeClass('show');
        }
    });
    
    // YouTube video
    $(window).bind('resize', function() {
        var $player = $('.player');
        var $iframe = $player.find('iframe');
        var width   = $player.width();
        var height  = $player.height();
        var tarWid  = width;
        var tarHei  = Math.round(tarWid * 9 / 16);
        if(tarHei < height) {
            tarHei  = height;
            tarWid  = Math.round(tarHei * 16 / 9);
        }
        var marTop  = Math.round((height - tarHei) / 2);
        var marLeft = Math.round((width - tarWid) / 2);
        $iframe.css({width: tarWid, height: tarHei, marginTop: marTop, marginLeft: marLeft});
    }).triggerHandler('resize');

})(jQuery);