//Delete Item Modal Action
$('.delete-item').click(function() {
    $('.btn-delete-item').attr("href", $(this).attr("href"));
    $("#deleteItemModal").modal('toggle');
    return false;
});

$(function() {

    $('#side-menu').metisMenu();

    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        language: 'pt-BR',
        autoclose: true
    });

    $(".data").mask("99/99/9999", { placeholder: "mm/dd/yyyy" });
    $(".phone").mask("(99) 9999-9999");
    $(".celphone").mask("(99) 9999-9999?9");
    $(".cep").mask("99999-999");
    $(".cpf").mask("999.999.999-99");

    $('.celphone').change(function() {
        if ($(this).val().replace(/[^0-9]/g, '').length >= 11) {
            $(".celphone").unmask();
            $(".celphone").mask("(99) 99999-999?9");
        } else {
            $(".celphone").unmask();
            $(".celphone").mask("(99) 9999-9999?9");
        }
    });


    $('.uppercase').bind('keydown keypress keyup', function(e) {
        var replacedText = $(this).val().replace(/[^A-Z0-9]/g, function(match) {
            if (match != undefined) {
                return match.toUpperCase();
            }
        });
        $(this).val(replacedText);
    });

    //Remove tudo que nao for letra, numero, ponto e arroba.
    //Validador de e-mail
    $('#email').bind('keydown keypress keyup', function(e) {
        var replacedText = $(this).val().replace(/[^a-zA-Z0-9\.@\-_]/g, function(match) {
            if (match != undefined) {
                return '';
            }
        });

        $(this).val(replacedText);
    });
    //Fechar div com mensagem de sessão
    $('#close-msg').bind('click', function() {
        $('#body-msg').fadeOut(100);
    });
    //Iniciar Colorbox
    $('a.colorbox').colorbox({ rel: 'img' });

});

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function() {
    $(window).bind("load resize", function() {
        topOffset = 50;
        width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse');
        }

        height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    });

    //var url = window.location;
    //var element = $('ul.nav a').filter(function() {
    //    return this.href == url || url.href.indexOf(this.href) == 0;
    //}).addClass('active').parent().parent().addClass('in').parent();
    //if (element.is('li')) {
    //    element.addClass('active');
    //}
});
