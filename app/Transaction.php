<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model {

    protected $table = 'transactions';
    
    protected $fillable = ['group_transaction_id', 'name', 'uri', 'method', 'action', 'module', 'private'];

    public function groups() {
        return $this->belongsToMany('App\Group', 'permissions', 'transaction_id', 'group_id');
    }
    
    public function grouptransaction() {
        return $this->belongsTo('App\GroupTransaction');
    }
    

}
