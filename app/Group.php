<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Group extends Model {

    use SoftDeletes;

    protected $table = 'groups';
    protected $dates = ['deleted_at'];
    protected $fillable = ['name'];

    public function users() {
        return $this->hasMany('App\User');
    }

    public function transactions() {
        return $this->belongsToMany('App\Transaction');
    }

}
