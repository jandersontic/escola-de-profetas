<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Event extends Model
{
    use SoftDeletes;

    protected $table = 'events';
    protected $dates = ['deleted_at', 'begin', 'end'];
    protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];

    public static function getAno($data)
    {
        $datas = explode('/', $data);
        if (is_array($datas)) {
            return end($datas);
        }
        return $data;
    }

    public function getBeginAttribute($value)
    {
        if (empty($value) || $value == '0000-00-00') {
            return null;
        } else {
            return Carbon::createFromFormat('Y-m-d', $value)->format('d/m/Y');
        }
    }

    public function setBeginAttribute($value)
    {
        if (!empty($value)) {
            $this->attributes['begin'] = Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
        }
    }

    public function getEndAttribute($value)
    {
        if (empty($value) || $value == '0000-00-00') {
            return null;
        } else {
            return Carbon::createFromFormat('Y-m-d', $value)->format('d/m/Y');
        }
    }

    public function setEndAttribute($value)
    {
        if (!empty($value)) {
            $this->attributes['end'] = Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
        }
    }

    public function prophets()
    {
        return $this->belongsToMany('App\Prophet');
    }

    public function midias()
    {
        return $this->hasMany('App\Midia');
    }
}
