<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Midia extends Model
{
	protected $table = 'midias';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function event()
    {
        return $this->belongsTo('App\Event');
    }
}
