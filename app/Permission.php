<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\Transaction;

class Permission extends Model
{
    protected $table = 'permissions';
    
    protected $fillable = ['group_id', 'transaction_id'];

    public function group() {
        return $this->belongsTo('App\Group');
    }
    
    public function transaction() {
        return $this->belongsTo('App\Transaction');
    }
    /**
     * Verifica se o usuário logado tem permissão para acessar a rota informada.
     * @param string $name Nome da rota a ser verificada.
     * @param int $group_id Id do grupo do usuário logado.
     * @return boolean Retorna true se o usuário logado pode acessar a rota ou retorna false caso contrário.
     */
    public static function verificaPermissao($name, $group_id = null)
    {
        if (!Auth::check()) {
            return false;
        }

        if (empty($group_id)) {
            $group_id = Auth::user()->group->id;
        }

        $transaction = Transaction::where('name', $name)->first();
        
        if (!empty($transaction) && $transaction->id) {
            
            $count = Permission::where('transaction_id', $transaction->id)->where('group_id', $group_id)->count();
            
            if ($count) {
                return true;
            }
        }
        
        return false;
    }
}
