<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Carbon\Carbon;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';
    
    protected $dates = ['deleted_at', 'birthdate'];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function setPostalCodeAttribute($value)
    {
        if (!empty($value)) {
            $this->attributes['postal_code'] = str_replace(['-', '.'], ['', ''], $value);
        }
    }

    public function getBirthdateAttribute($value)
    {
        if (empty($value) || $value == '0000-00-00') {
            return null;
        } else {
            return Carbon::createFromFormat('Y-m-d', $value)->format('d/m/Y');
        }
    }

    public function setBirthdateAttribute($value)
    {
        if (!empty($value)) {
            $this->attributes['birthdate'] = Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
        }
    }
    
    public function group()
    {
        return $this->belongsTo('App\Group');
    }

}
