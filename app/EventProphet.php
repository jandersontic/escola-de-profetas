<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventProphet extends Model
{
    protected $table = 'event_prophet';
    protected $fillable = ['event_id', 'prophet_id'];

    public function event() {
        return $this->belongsTo('App\Event');
    }

    public function prophet() {
        return $this->belongsTo('App\Prophet');
    }
}
