<?php

namespace App\Http\Middleware;

use Closure;

class Permissions
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $action = $request->route()->getAction();
        
        if ($request->user()) {
            if (!session('permissions')[$action['as']]) {
                return view('errors.401');
            }
        }

        return $next($request);
    }
}
