<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\EventProphet;
use App\Event;
use App\Banner;
use App\Quote;
use App\User;
use App\Prophet;
use Mail;
use Validator;

class IndexController extends Controller
{

    public function index()
    {
        //Adicionar imagem de fundo na seção "sobre"
        $data = ['banners','sobre', 'prophets', 'event', 'quotes'];

        $event = Event::where('active', true)->first();
        $banners = Banner::where('active', true)->orderBy('order', 'asc')->orderBy('id', 'asc')->get();
        $quotes = Quote::where('active', true)->orderBy('order', 'asc')->orderBy('id', 'asc')->get();

        if ($event and $event->id) {
            $prophets = $event->prophets()->orderBy('name', 'desc')->orderBy('id', 'asc')->get();
        } else {
            $prophets = [];
        }

        return view('site.index.index', compact($data));
    }

    public function contato(Request $request)
    {
        $input = $request->only(['name', 'email', 'subject', 'bodyMessage']);
        $validator = Validator::make($input, [
            'name' => 'required',
            'email' => 'required|email',
            'subject' => 'required',
            'bodyMessage' => 'required|max:255'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 2, 'msg' => 'Por gentileza, preencha todos os campos.']);
        }

        Mail::send('emails.contato', $input, function ($m) use ($input) {
            $m->from('noreply@escoladeprofetasbelem.com.br', 'Escola de Profetas Belém');
            $m->to('contato@escoladeprofetasbelem.com.br', 'Escola de Profetas');
            $m->bcc('janderson.tic@gmail.com', 'Janderson Silva');
            $m->replyTo($input['email'], $input['name']);
            $m->subject('Atendimento - Escola de Profetas Belém');
        });

        return response()->json(['status' => 1, 'msg' => 'Obrigado por entrar em contato conosco. Retornaremos seu contato o mais breve possível.']);
    }

    public function cadastro(Request $request)
    {
        $input = $request->except('_token', 'save');

        $validator = Validator::make($input, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ], [
            'name.required' => 'Nós precisamos saber o seu nome.',
            'name.max' => 'O nome deverá conter no máximo :max caracteres.',
            'email.required' => 'Nós precisamos saber o seu e-mail.',
            'email.email' => 'Informe um e-mail válido.',
            'email.unique' => 'Este e-mail já foi cadastrado por outro usuário.',
            'email.max' => 'O e-mail deverá conter no máximo :max caracteres.',
            'password.required' => 'É necessário criar uma senha.',
            'password.confirmed' => 'A confirmação da senha não corresponde.',
            'password.min' => 'A sua senha deverá conter no mínimo :min caracteres.'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 2, 'msg' => implode('<br>', $validator->errors()->all())]);
        }

        $input['password'] = bcrypt($input['password']);
        unset($input['password_confirmation']);
        //USUARIOS CADASTRADOS VIA SITE SEMPRE SERAO DO GRUPO 2 => ALUNO
        $input['group_id'] = 2;
        $user = User::create($input);

        if ($user && $user->id) {
            return response()->json([
                'status' => 1,
                'msg' => 'Cadastro realizado.'
                ]);
        }

        return response()->json(['status' => 2, 'msg' => 'Cadastro não realizado!']);
    }
}