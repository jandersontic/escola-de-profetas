<?php

namespace App\Http\Controllers\Controle;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Group;
use Validator;

class UserController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $input = $request->except('_token', 'save');

        $data = ['users'];
        if (array_key_exists('aniversario', $input)) {
            if (is_numeric($input['aniversario']) && $input['aniversario'] > 0 && $input['aniversario'] < 13) {
                $users = User::whereRaw('month(birthdate) = ' . $input['aniversario'])->paginate(10);
            } else {
                $first = strtotime('midnight first day of this month');
                $users = User::whereRaw('month(birthdate) = ' . date('m', $first))->paginate(10);
            }
        } else {
            $users = User::orderBy('id')->paginate(10);
        }
        return view('controle.user.index', compact($data));
    }

    public function create()
    {
        $data = ['groups'];
        $groups = Group::orderBy('id')->get();

        return view('controle.user.create', compact($data));
    }

    public function store(Request $request)
    {
        $input = $request->except('_token', 'save');

        $validator = Validator::make($input, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
            'group_id' => 'required',
        ], [
            'name' => [
                'required' => 'Campo Nome é obrigatório.',
                'max' => 'Campo Nome deverá conter no máximo :max caracteres.'
            ],
            'email' => [
                'required' => 'Campo E-mail é obrigatório.',
                'email' => 'Informe um E-mail válido.',
                'max' => 'Campo E-mail deverá conter no máximo :max caracteres.'
            ],
            'password' => [
                'required' => 'Campo Senha é obrigatório.',
                'confirmed' => 'A Confirmação da Senha não corresponde.',
                'min' => 'Campo Senha deverá conter no mínimo :min caracteres.'
            ],
            'group_id' => [
                'required' => 'Campo Grupo é obrigatório.',
                'not_in' => 'Campo Grupo obrigatório.'
            ]
        ]);

        if ($validator->fails()) {
            return \Redirect::route('controle.user.create')->with('errors', true)->with('msg', implode('<br>', $validator->errors()->all()))->withInputs();
        }

        $input['password'] = bcrypt($input['password']);
        unset($input['password_confirmation']);
        $user = User::create($input);

        if ($user->id) {
            return \Redirect::route('controle.user.create')->with('errors', false)->with('msg', 'Operação realizada com sucesso!');
        }

        return \Redirect::route('controle.user.create')->with('errors', true)->with('msg', 'Não foi possível realizar a operação!')->withInputs();;
    }

    public function show($id)
    {
        $data = ['id', 'user'];
        $user = User::find($id);

        if ($id && $user->id) {
            return view('controle.user.show', compact($data));
        }

        return \Redirect::route('controle.user.index')->with('errors', false)->with('msg', 'Usuário não localizado!');
    }

    public function edit($id)
    {
        $data = ['id', 'user', 'groups'];
        $groups = Group::orderBy('id')->get();
        $user = User::find($id);

        if ($id && $user->id) {
            return view('controle.user.edit', compact($data));
        }

        return \Redirect::route('controle.user.index')->with('errors', false)->with('msg', 'Usuário não localizado!');
    }

    public function update(Request $request, $id)
    {
        $input = $request->except('_token', 'save');
        $user = User::find($id);

        if ($id && $user->id) {
            if ($input['password']) {
                $validator = Validator::make($input, [
                            'name' => 'required|max:255',
                            'email' => 'required|email|max:255',
                            'password' => 'required|confirmed|min:6',
                            'group_id' => 'required|not_in:0',
                                ],
                        [
                            'name' => [
                                'required' => 'Campo Nome é obrigatório.',
                                'max' => 'Campo Nome deverá conter no máximo :max caracteres.'
                            ],
                            'email' => [
                                'required' => 'Campo E-mail é obrigatório.',
                                'email' => 'Informe um E-mail válido.',
                                'max' => 'Campo E-mail deverá conter no máximo :max caracteres.'
                            ],
                            'password' => [
                                'required' => 'Campo Senha é obrigatório.',
                                'confirmed' => 'A Confirmação da Senha não corresponde.',
                                'min' => 'Campo Senha deverá conter no mínimo :min caracteres.'
                            ],
                            'group_id' => [
                                'required' => 'Campo Grupo é obrigatório.',
                                'not_in' => 'Campo Grupo obrigatório.'
                            ]
                        ]);
                $input['password'] = bcrypt($input['password']);
            } else {
                unset($input['password']);
                unset($input['password_confirmation']);
                $validator = Validator::make($input, [
                            'name' => 'required|max:255',
                            'email' => 'required|email|max:255',
                            'group_id' => 'required|not_in:0',
                                ],
                        [
                            'name' => [
                                'required' => 'Campo Nome é obrigatório.',
                                'max' => 'Campo Nome deverá conter no máximo :max caracteres.'
                            ],
                            'email' => [
                                'required' => 'Campo E-mail é obrigatório.',
                                'email' => 'Informe um E-mail válido.',
                                'max' => 'Campo E-mail deverá conter no máximo :max caracteres.'
                            ],
                            'group_id' => [
                                'required' => 'Campo Grupo é obrigatório.',
                                'not_in' => 'Campo Grupo obrigatório.'
                            ]
                        ]);
            }

            if ($validator->fails()) {
                return \Redirect::route('controle.user.edit', $id)->with('errors', true)->with('msg', implode('<br>', $validator->errors()->all()));
            }

            if ($user->update($input)) {
                return \Redirect::route('controle.user.edit', $id)->with('errors', false)->with('msg', 'Operação realizada com sucesso!');
            }
        }

        return \Redirect::route('controle.user.edit', $id)->with('errors', true)->with('msg', 'Não foi possível realizar a operação!');
    }

    public function delete($id) {
        $user = User::find($id);
        if ($user->id && $user->delete()) {
            return \Redirect::route('controle.user.index')->with('errors', false)->with('msg', 'Operação realizada com sucesso');
        }
        return \Redirect::route('controle.user.index')->with('errors', true)->with('msg', 'Não foi possível realizar a operação!');
    }

    /**
   * Visualizar formulário para alterar perfil do usuário logado.
   * @Post("getprofile", as="controle.user.getprofile")
   * @return Response
   */
    public function getprofile()
    {
        if (Auth::check()) {
            $data = ['user'];
            $id = Auth::user()->id;
            $user = User::find($id);

            if ($id && $user->id) {
                return view('controle.user.profile', compact($data));
            }
        }

        return \Redirect::route('controle.auth.logout')->with('errors', true)->with('msg', 'Entre para editar seu perfil.');
    }

    public function postprofile(Request $request)
    {
        if (!Auth::check()) {
            return \Redirect::route('controle.auth.logout')->with('errors', true)->with('msg', 'Entre para editar seu perfil.');
        }

        $id = Auth::user()->id;
        $input = $request->except('_token', 'save');
        $user = User::find($id);

        if ($id && $user->id) {
            if ($input['password']) {
                $validator = Validator::make(
                    $input, 
                    [
                        'name' => 'required|max:255',
                        'email' => 'required|email|max:255',
                        'password' => 'required|confirmed|min:6',
                    ],
                    [
                        'name' => [
                            'required' => 'Campo Nome é obrigatório.',
                            'max' => 'Campo Nome deverá conter no máximo :max caracteres.'
                        ],
                        'email' => [
                            'required' => 'Campo E-mail é obrigatório.',
                            'email' => 'Informe um E-mail válido.',
                            'max' => 'Campo E-mail deverá conter no máximo :max caracteres.'
                        ],
                        'password' => [
                            'required' => 'Campo Senha é obrigatório.',
                            'confirmed' => 'A Confirmação da Senha não corresponde.',
                            'min' => 'Campo Senha deverá conter no mínimo :min caracteres.'
                        ]
                    ]);
                $input['password'] = bcrypt($input['password']);
            } else {
                unset($input['password']);
                $validator = Validator::make(
                    $input,
                    [
                        'name' => 'required|max:255',
                        'email' => 'required|email|max:255'
                    ],  
                    [
                        'name' => [
                            'required' => 'Campo Nome é obrigatório.',
                            'max' => 'Campo Nome deverá conter no máximo :max caracteres.'
                        ],
                        'email' => [
                            'required' => 'Campo E-mail é obrigatório.',
                            'email' => 'Informe um E-mail válido.',
                            'max' => 'Campo E-mail deverá conter no máximo :max caracteres.'
                        ]
                    ]);
            }

            if ($validator->fails()) {
                return \Redirect::route('controle.user.getprofile')->with('errors', true)->with('msg', implode('<br>', $validator->errors()->all()));
            }

            unset($input['password_confirmation']);
            
            if ($user->update($input)) {
                return \Redirect::route('controle.user.getprofile')->with('errors', false)->with('msg', 'Perfil salvo.');
            }
        }

        return \Redirect::route('controle.auth.logout')->with('errors', true)->with('msg', 'Entre para editar seu perfil.');
    }

}
