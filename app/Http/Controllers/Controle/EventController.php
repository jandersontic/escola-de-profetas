<?php

namespace App\Http\Controllers\Controle;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Event;
use App\Prophet;
use App\EventProphet;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = ['events'];
        $events = Event::orderBy('id', 'desc')->get();
        return view('controle.event.index', compact($data));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = ['prophets'];
        $prophets = Prophet::orderBy('name', 'desc')->orderBy('id', 'asc')->get();
        return view('controle.event.create', compact($data));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->except(['_token', 'save']);
        $prophets = array_key_exists('prophets', $input) ? $input['prophets'] : [];
        unset($input['prophets']);
        $event = Event::create($input);

        if ($event && $event->id) {
            if (is_array($prophets) && count($prophets)) {
                foreach ($prophets as $prophet) {
                    $prophetCreated = EventProphet::firstOrCreate(['prophet_id' => $prophet, 'event_id' => $event->id]);
                }
            }
            return \Redirect::route('controle.event.create')->with('errors', false)->with('msg', 'Evento salvo.');
        }

        return \Redirect::route('controle.event.create')->with('errors', true)->with('msg', 'Erro ao salvar Evento!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = ['id', 'event'];
        $event = Event::find($id);

        if ($event && $event->id) {
            return view('controle.event.show', compact($data));
        }

        return \Redirect::route('controle.event.index')->with('errors', false)->with('msg', 'Evento não localizado!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = ['id', 'event', 'prophets'];
        $event = Event::find($id);
        $prophets = Prophet::orderBy('name', 'desc')->orderBy('id', 'asc')->get();

        if ($event && $event->id) {
            return view('controle.event.edit', compact($data));
        }

        return \Redirect::route('controle.event.index')->with('errors', true)->with('msg', 'Evento não localizado!');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->except(['_token', 'save']);
        $event = Event::find($id);
        $prophets = array_key_exists('prophets', $input) ? $input['prophets'] : [];
        unset($input['prophets']);

        if ($event && $event->id && $event->update($input)) {

            $eventProphets = EventProphet::where('event_id', $event->id)->get();
            foreach ($eventProphets as $eventProphet) {
                $eventProphet->delete();
            }

            if (is_array($prophets) && count($prophets)) {
                foreach ($prophets as $prophet) {
                    $prophetCreated = EventProphet::firstOrCreate(['prophet_id' => $prophet, 'event_id' => $event->id]);
                }
            }

            return \Redirect::route('controle.event.edit', $id)->with('errors', false)->with('msg', 'Evento atualizado.');
        }

        return \Redirect::route('controle.event.edit', $id)->with('errors', true)->with('msg', 'Erro ao atualizar evento!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event = Event::find($id);

        if ($event && $event->id && $event->delete()) {
            return \Redirect::route('controle.event.index')->with('errors', false)->with('msg', 'Operação realizada com sucesso');
        }

        return \Redirect::route('controle.event.index')->with('errors', true)->with('msg', 'Não foi possível realizar a operação!');
    }
}
