<?php

namespace App\Http\Controllers\Controle;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Banner;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = ['banners'];
        $banners = Banner::orderBy('order', 'asc')->orderBy('id', 'desc')->get();
        return view('controle.banner.index', compact($data));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('controle.banner.create');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = ['id', 'banner'];
        $banner = Banner::find($id);

        if ($banner && $banner->id) {
            return view('controle.banner.edit', compact($data));
        }

        return \Redirect::route('controle.banner.index')->with('errors', true)->with('msg', 'Banner não localizado!');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!$request->hasFile('image') or !$request->file('image')->isValid()) {
            return \Redirect::route('controle.banner.create')->with('errors', true)->with('msg', 'Não foi possível enviar o arquivo!');
        }

        try {
            $fileName = md5(uniqid() . date('YmdHis')) . '.' . $request->file('image')->getClientOriginalExtension();
            $request->file('image')->move(public_path() . '/data/banner/', $fileName);
        } catch (FileException $e) {
            return \Redirect::route('controle.banner.create')->with('errors', true)->with('msg', 'Não foi possível mover o arquivo!');
        }

        $input = $request->except(['_token', 'save']);
        $input['image'] = $fileName;

        $banner = Banner::create($input);

        if ($banner && $banner->id) {
            return \Redirect::route('controle.banner.create')->with('errors', false)->with('msg', 'Operação realizada com sucesso!');
        }

        return \Redirect::route('controle.banner.create')->with('errors', true)->with('msg', 'Não foi possível realizar a operação!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = ['id', 'banner'];
        $banner = Banner::find($id);

        if ($banner && $banner->id) {
            return view('controle.banner.show', compact($data));
        }

        return \Redirect::route('controle.banner.index')->with('errors', false)->with('msg', 'Banner não localizado!');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->hasFile('image') and $request->file('image')->isValid()) {
            //substituir imagem salva no banco
            try {
                $fileName = md5(uniqid() . date('YmdHis')) . '.' . $request->file('image')->getClientOriginalExtension();
                $request->file('image')->move(public_path() . '/data/banner/', $fileName);
            } catch (FileException $e) {
                return \Redirect::route('controle.banner.create')->with('errors', true)->with('msg', 'Não foi possível mover o arquivo!');
            }
        } else {
            //permanecer a mesma imagem salva no banco
            $fileName = null;
        }

        $input = $request->except(['_token', 'save']);
        $banner = Banner::find($id);
        $input['image'] = empty($fileName) ? $banner->image : $fileName;

        if ($banner && $banner->id && $banner->update($input)) {
            return \Redirect::route('controle.banner.edit', $id)->with('errors', false)->with('msg', 'Operação realizada com sucesso!');
        }

        return \Redirect::route('controle.banner.edit', $id)->with('errors', true)->with('msg', 'Não foi possível realizar a operação!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $banner = Banner::find($id);

        if ($banner && $banner->id && $banner->delete()) {
            return \Redirect::route('controle.banner.index')->with('errors', false)->with('msg', 'Operação realizada com sucesso');
        }

        return \Redirect::route('controle.banner.index')->with('errors', true)->with('msg', 'Não foi possível realizar a operação!');
    }

    public function destroyimage($id)
    {
        $banner = Banner::find($id);
        $input['image'] = '';
        $fileDeleted = true;
        if (is_file(public_path() . '/data/banner/' . $banner->image)) {
            $fileDeleted = @unlink(public_path() . '/data/banner/' . $banner->image);
        }
        if ($banner && $banner->id && $fileDeleted && $banner->update($input)) {
            return \Redirect::route('controle.banner.edit', $id)->with('errors', false)->with('msg', 'Operação realizada com sucesso');
        }

        return \Redirect::route('controle.banner.edit', $id)->with('errors', true)->with('msg', 'Não foi possível realizar a operação!');
    }
}
