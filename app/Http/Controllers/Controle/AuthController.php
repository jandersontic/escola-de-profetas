<?php

namespace App\Http\Controllers\Controle;

use App\Permission;
use App\Transaction;
use Illuminate\Support\Facades\Input;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    protected $redirectTo = '/controle';
    
    // protected $loginPath = '/controle/auth/login';
    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }
    
    public function login()
    {
        return view('controle.auth.login');
    }

    public function logar(Request $request)
    {
        $input = $request->only('email', 'password');

        $validator = Validator::make($input, [
            'email' => 'required', 'password' => 'required'
        ]);

        if (!$validator->fails() && Auth::attempt($input, $request->has('remember'))) {
            session(['permissions' => $this->permissions()]);
            return redirect()->intended($this->redirectPath())->withInput();
        }

        return redirect()->route('controle.auth.login')->with('errors', true)->with('msg', 'O e-mail e a senha que você digitou não coincidem.')->withInput();
    }
    
    public function permissions()
    {
        $routers = [];
        $transactions = Transaction::where('private', true)->where('module', 'Controle')->get();
        
        foreach($transactions as $transaction) {
            $routers[$transaction->name] = Permission::where('group_id', Auth::user()->group->id)->where('transaction_id', $transaction->id)->count() ? true : false;
        }
        
        return $routers;
    }

    public function logout()
    {
        Auth::logout();
        return \Redirect::route('controle.auth.login');
    }
}
