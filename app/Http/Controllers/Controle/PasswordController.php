<?php

namespace App\Http\Controllers\Controle;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Password;
use Validator;
use App\Http\Controllers\Controle\AuthController;
use Illuminate\Support\Facades\Auth;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;
    protected $redirectTo = '/controle';
    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function getEmail()
    {
        return view('controle.auth.password');
    }

    public function postEmail(Request $request)
    {
        $input = $request->only('email');
        $validator = Validator::make($input, [
            'email' => 'required|email'
        ]);

        if ($validator->fails()) {
            return redirect()->route('controle.password.getemail')->with('errors', true)->with('msg', 'E-mail inválido.')->withInput();
        }

        $response = Password::sendResetLink($input, function (Message $m, $input) {
            $m->from('noreply@escoladeprofetasbelem.com.br', 'Escola de Profetas Belém');
            $m->to('janderson.tic@gmail.com', 'Janderson Silva');
            $m->subject('Recuperar Senha - Escola de Profetas');
        });

        switch ($response) {
            case Password::RESET_LINK_SENT:
                return redirect()->route('controle.auth.login')
                    ->with('errors', false)
                    ->with('msg', 'Nós enviamos um email para <b>' . $input['email'] . '</b>. Siga as instruções para recuperar a sua senha.');

            case Password::INVALID_USER:
                return redirect()->route('controle.password.getemail')
                    ->with('errors', true)
                    ->with('msg', 'Ops! Não conseguimos enviar um e-mail para você com as instruções de recuperação de senha. Por favor realize uma nova tentativa em algumas horas.');
        }
    }

    public function getReset($token = null)
    {
        if (is_null($token)) {
            throw new NotFoundHttpException;
        }

        return view('controle.auth.reset')->with('token', $token);
    }

    public function postReset(Request $request)
    {
        $credentials = $request->only(
            'email', 'password', 'password_confirmation', 'token'
        );

        $validator = Validator::make($credentials, [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:6',
        ]);

        $response = Password::reset($credentials, function ($user, $password) {
            $this->resetPassword($user, $password);
        });
        
        switch ($response) {
            case Password::PASSWORD_RESET:
                return redirect($this->redirectPath())->with('status', trans($response));
            default:
                return redirect()->route('controle.password.getreset', $credentials['token'])
                            ->with('errors', true)
                            ->with('msg', trans($response))
                            ->withInput($request->only('email'));
        }
    }

    protected function resetPassword($user, $password)
    {
        $user->password = bcrypt($password);
        $user->save();
        Auth::login($user);
        $authController = new AuthController();
        session(['permissions' => $authController->permissions()]);
    }
}
