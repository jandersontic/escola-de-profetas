<?php

namespace App\Http\Controllers\Controle;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Permission;
use App\Http\Requests;
use App\Transaction;
use App\GroupTransaction;
use App\Group;

class PermissionController extends Controller
{
    public function index(Request $request)
    {
        $data = ['group_id', 'groups', 'groupTransactions', 'permissions'];
        
        $group_id = (int) $request->only(['group_id'])['group_id'];
        $groups = Group::orderBy('id')->get();
        if ($group_id) {
            $groupTransactions = GroupTransaction::where('id', '>', 1)->orderBy('id')->get();
            $transactions = Transaction::where('private', true)->get();
            foreach ($transactions as $transaction) {
                $permissions[$transaction->id] = '';
                if (Permission::verificaPermissao($transaction->name, $group_id)) {
                    $permissions[$transaction->id] = 'checked';
                }
            }
        }

        return view('controle.permission.index', compact($data));
    }
    
    public function save(Request $request, $id)
    {
        $input = $request->except(['_token']);
        
        $permissions = Permission::where('group_id', $id)->get();
        
        if (count($permissions)) {
            foreach ($permissions as $permission) {
                if (!array_key_exists($permission->transaction()->first()->name, $input)) {
                    $permission->delete();
                }
            }
        }
        
        foreach ($input as $name) {
            $transaction = Transaction::where('name', $name)->first();
            if (!$transaction || !$transaction->id || !$id) {
                continue;
            }
            $permission = Permission::firstOrCreate(['group_id' => $id, 'transaction_id' => $transaction->id]);
            if (!$permission) {
                return \Redirect::route('controle.permission.index')->with('group_id', $id)->with('errors', true)->with('msg', 'Não foi possível realizar a operação!');
            }
        }
        
        return \Redirect::route('controle.permission.index')->with('group_id', $id)->with('errors', false)->with('msg', 'Operação realizada com sucesso!');
    }
}