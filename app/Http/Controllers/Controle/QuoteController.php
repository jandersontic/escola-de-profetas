<?php

namespace App\Http\Controllers\Controle;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Quote;

class QuoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = ['quotes'];
        $quotes = Quote::orderBy('id', 'desc')->paginate(15);
        return view('controle.quote.index', compact($data));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('controle.quote.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->except(['_token', 'save']);
        $quote = Quote::create($input);

        if ($quote && $quote->id) {
            return \Redirect::route('controle.quote.create')->with('errors', false)->with('msg', 'Operação realizada com sucesso!');
        }

        return \Redirect::route('controle.quote.create')->with('errors', true)->with('msg', 'Não foi possível realizar a operação!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = ['id', 'quote'];
        $quote = Quote::find($id);

        if ($quote && $quote->id) {
            return view('controle.quote.show', compact($data));
        }

        return \Redirect::route('controle.quote.index')->with('errors', false)->with('msg', 'Evento não localizado!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = ['id', 'quote'];
        $quote = Quote::find($id);

        if ($quote && $quote->id) {
            return view('controle.quote.edit', compact($data));
        }

        return \Redirect::route('controle.quote.index')->with('errors', true)->with('msg', 'Evento não localizado!');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->except(['_token', 'save']);
        $quote = Quote::find($id);

        if ($quote && $quote->id && $quote->update($input)) {
            return \Redirect::route('controle.quote.edit', $id)->with('errors', false)->with('msg', 'Operação realizada com sucesso!');
        }

        return \Redirect::route('controle.quote.edit', $id)->with('errors', true)->with('msg', 'Não foi possível realizar a operação!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $quote = Quote::find($id);

        if ($quote && $quote->id && $quote->delete()) {
            return \Redirect::route('controle.quote.index')->with('errors', false)->with('msg', 'Operação realizada com sucesso');
        }

        return \Redirect::route('controle.quote.index')->with('errors', true)->with('msg', 'Não foi possível realizar a operação!');
    }
}
