<?php

namespace App\Http\Controllers\Controle;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Prophet;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use App\Library\M2brimagem;

class ProphetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = ['prophets'];
        $prophets = Prophet::orderBy('name', 'asc')->orderBy('id', 'desc')->get();
        return view('controle.prophet.index', compact($data));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('controle.prophet.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            try {
                $fileName = md5(uniqid() . date('YmdHis')) . '.' . $request->file('image')->getClientOriginalExtension();
                $request->file('image')->move(public_path() . '/data/preletor/', $fileName);
            } catch (FileException $e) {
                return \Redirect::route('controle.prophet.create')->with('errors', true)->with('msg', 'Erro ao salvar a imagem!');
            }

            $input = $request->except(['_token', 'save']);
            $input['image'] = $fileName;
        } else {
            $input = $request->except(['_token', 'save', 'image']);
        }

        $prophet = Prophet::create($input);

        if ($prophet && $prophet->id) {
            return \Redirect::route('controle.prophet.create')->with('errors', false)->with('msg', 'Preletor salvo.');
        }

        return \Redirect::route('controle.prophet.create')->with('errors', true)->with('msg', 'Erro ao salvar preletor!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = ['id', 'prophet'];
        $prophet = Prophet::find($id);

        if ($prophet && $prophet->id) {
            return view('controle.prophet.show', compact($data));
        }

        return \Redirect::route('controle.prophet.index')->with('errors', true)->with('msg', 'Preletor não localizado!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = ['id', 'prophet'];
        $prophet = Prophet::find($id);

        if ($prophet && $prophet->id) {
            return view('controle.prophet.edit', compact($data));
        }

        return \Redirect::route('controle.prophet.index')->with('errors', true)->with('msg', 'Preletor não localizado!');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $changeImage = false;
        if ($request->hasFile('image') and $request->file('image')->isValid()) {
            //substituir imagem salva no banco
            try {
                $fileName = md5(uniqid() . date('YmdHis')) . '.' . $request->file('image')->getClientOriginalExtension();
                $request->file('image')->move(public_path() . '/data/preletor/', $fileName);
                $changeImage = true;
            } catch (FileException $e) {
                return \Redirect::route('controle.prophet.create')->with('errors', true)->with('msg', 'Erro salvar a imagem!');
            }
            $input = $request->except(['_token', 'save']);
            $input['image'] = $fileName;
        } else {
            //permanecer a mesma imagem salva no banco
            $input = $request->except(['_token', 'save', 'image']);
            unset($input['image']);
        }

        $prophet = Prophet::find($id);

        if ($prophet && $prophet->id && $prophet->update($input)) {
            if ($changeImage && $prophet->image != $input['image'] && is_file(public_path() . '/data/preletor/' . $prophet->image)) {
                @unlink(public_path() . '/data/preletor/' . $prophet->image);
            }
            return \Redirect::route('controle.prophet.edit', $id)->with('errors', false)->with('msg', 'Preletor atualizado.');
        }

        return \Redirect::route('controle.prophet.edit', $id)->with('errors', true)->with('msg', 'Erro ao atualizar preletor!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $prophet = Prophet::find($id);

        if ($prophet && $prophet->id && $prophet->delete()) {
            return \Redirect::route('controle.prophet.index')->with('errors', false)->with('msg', 'Preletor deletado.');
        }

        return \Redirect::route('controle.prophet.index')->with('errors', true)->with('msg', 'Erro ao deletar preletor!');
    }

    public function destroyimage($id)
    {
        $prophet = Prophet::find($id);
        $input['image'] = '';
        $fileDeleted = true;

        if ($prophet && $prophet->id) {
            if (is_file(public_path() . '/data/preletor/' . $prophet->image)) {
                $fileDeleted = @unlink(public_path() . '/data/preletor/' . $prophet->image);
            }
            if ($prophet && $prophet->id && $fileDeleted && $prophet->update($input)) {
                return \Redirect::route('controle.prophet.edit', $id)->with('errors', false)->with('msg', 'Imagem deletada.');
            }
        }

        return \Redirect::route('controle.prophet.edit', $id)->with('errors', true)->with('msg', 'Erro ao deletar imagem!');
    }

    public function image($id)
    {
        $data = ['id', 'prophet'];
        $prophet = Prophet::find($id);

        if ($prophet && $prophet->id) {
            return view('controle.prophet.crop', compact($data));
        }

        return \Redirect::route('controle.prophet.index')->with('errors', true)->with('msg', 'Preletor não localizado!');
    }

    public function cropimage(Request $request, $id)
    {
        $input = $request->except(['_token', 'save']);
        $prophet = Prophet::find($id);
        $error = true;
        if ($prophet && $prophet->id && $prophet->image) {
            $path = public_path() . '/data/preletor/' . $prophet->image;
            $pathCrop = public_path() . '/data/preletor/cut-' . $prophet->image;
            //redimensiona a imagem conforme resolucao do disposivito do usuario
            $imagem = new M2brimagem();
            $imagem->start($path);
            if ($imagem->valida() == 'OK') {
                $imagem->redimensiona($input['imageWidth'], $input['imageHeight']);
                $imagem->grava($path);
                $imagem = null;
                $error = false;
            }
            //cortar imagem
            $imagem = new M2brimagem();
            $imagem->start($path);
            if (!$error && $imagem->valida() == 'OK') {
                $imagem->posicaoCrop($input['x'], $input['y'], $input['w'], $input['h']);
                $imagem->redimensiona(263, 280, 'crop');
                $imagem->grava($pathCrop, 80);
                $imagem = null;
                $error = false;
            }
        }

        if ($error) {
            return \Redirect::route('controle.prophet.edit', $id)->with('errors', true)->with('msg', 'Erro ao cortar imagem!');
        }
        return \Redirect::route('controle.prophet.edit', $id)->with('errors', false)->with('msg', 'Imagem cortada.');

    }
}
