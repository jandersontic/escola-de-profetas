<?php

namespace App\Http\Controllers\Controle;

use App\Http\Controllers\Controller;

class IndexController extends Controller
{

    public function index()
    {
        return view('controle.index.index', ['mensagem' => 'Olá mundo!']);
    }
}