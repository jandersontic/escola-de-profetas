<?php

namespace App\Http\Controllers\Controle;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Group;

class GroupController extends Controller
{

    public function index(Request $request)
    {
        $data = ['groups'];
        $groups = Group::orderBy('id')->get();
        return view('controle.group.index', compact($data));
    }

    public function create()
    {
        return view('controle.group.create');
    }

    public function store(Request $request)
    {
        $input = $request->only('name');
        $group = Group::create($input);

        if ($group && $group->id) {
            return \Redirect::route('controle.group.create')->with('errors', false)->with('msg', 'Operação realizada com sucesso!');
        }

        return \Redirect::route('controle.group.create')->with('errors', true)->with('msg', 'Não foi possível realizar a operação!');
    }

    public function show($id)
    {
        $data = ['id', 'group'];
        $group = Group::find($id);

        if ($group && $group->id) {
            return view('controle.group.show', compact($data));
        }

        return \Redirect::route('controle.group.index')->with('errors', false)->with('msg', 'Grupo não localizado!');
    }

    public function edit($id)
    {
        $data = ['id', 'group'];
        $group = Group::find($id);

        if ($group && $group->id) {
            return view('controle.group.edit', compact($data));
        }

        return \Redirect::route('controle.group.index')->with('errors', true)->with('msg', 'Grupo não localizado!');
    }
    
    public function update(Request $request, $id)
    {
        $input = $request->only('name');
        $group = Group::find($id);

        if ($group && $group->id && $group->update($input)) {
            return \Redirect::route('controle.group.edit', $id)->with('errors', false)->with('msg', 'Operação realizada com sucesso!');
        }

        return \Redirect::route('controle.group.edit', $id)->with('errors', true)->with('msg', 'Não foi possível realizar a operação!');
    }
    
    public function destroy($id)
    {
        $group = Group::find($id);

        if ($group && $group->id && $group->delete()) {
            return \Redirect::route('controle.group.index')->with('errors', false)->with('msg', 'Operação realizada com sucesso');
        }

        return \Redirect::route('controle.group.index')->with('errors', true)->with('msg', 'Não foi possível realizar a operação!');
    }
}