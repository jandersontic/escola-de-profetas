<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

Route::group(['middleware' => 'guest'], function () {
    $publicRoutes = App\Transaction::where('private', false)->where('module', 'Site')->get();
    foreach ($publicRoutes as $route) {
      $parametros = ['as' => $route->name, 'uses' => $route->method];
      switch ($route->type) {
            case 'get':
              Route::get($route->uri, $parametros);
              break;
            case 'post':
              Route::post($route->uri, $parametros);
              break;
            case 'put':
              Route::put($route->uri, $parametros);
              break;
            case 'delete':
              Route::delete($route->uri, $parametros);
              break;
            default:
              Route::any($route->uri, $parametros);
              break;
          }
    }
});

Route::group(['prefix' => 'controle'], function () {
    
    $privateRoutes = App\Transaction::where('module', 'Controle')->get();

    foreach ($privateRoutes as $rota) {
        if ($rota->private) {
          $parametros = ['middleware' => ['auth', 'permissions'],'as' => $rota->name, 'uses' => $rota->method]; 
        } else {
          $parametros = ['as' => $rota->name, 'uses' => $rota->method];
        }

        switch ($rota->type) {
            case 'get':
              Route::get($rota->uri, $parametros);
              break;
            case 'post':
              Route::post($rota->uri, $parametros);
              break;
            case 'put':
              Route::put($rota->uri, $parametros);
              break;
            case 'delete':
              Route::delete($rota->uri, $parametros);
              break;
            default:
              Route::any($rota->uri, $parametros);
              break;
          }
    }
});
