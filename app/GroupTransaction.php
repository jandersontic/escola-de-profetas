<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupTransaction extends Model
{
    protected $table = 'group_transaction';
    
    protected $fillable = ['parent_id', 'name', 'icon', 'order'];

    public function transactions() {
        return $this->hasMany('App\Transaction');
    }
}
