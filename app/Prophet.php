<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Prophet extends Model
{
    use SoftDeletes;
    protected $table = 'prophets';
    protected $dates = ['deleted_at'];
    protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];

    public function events()
    {
        return $this->belongsToMany('App\Event');
    }
}
